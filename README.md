# MarioCorona
Projet de programmation logiciel en Qt, dans le cadre du second semestre de l'année 4 à Polytech Tours spécialité informatique.

## Participants du projet :
* Jacob Ador
* Pedro Da Rocha
* Corentin Fossaert

## Resumé du jeu : 
MarioCorona est un "Mario Like", où le but est de progresser dans les divers niveaux tout en éliminant les virus. Ramassez des masques pour gagner des vies !
Le niveau final vous oppose au méchant PANGOLIN.

## Commandes du jeu : 
* flèche de gauche pour aller à gauche.
* flèche de droite pour aller à droite.
* barre d'espace ou flèche du haut pour sauter.
* shif pour courir plus vite.
* escape pour mettre en pause.
* backspace pour fermer le jeu (ou croix rouge).