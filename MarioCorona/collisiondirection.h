#ifndef COLLISIONDIRECTION_H
#define COLLISIONDIRECTION_H
#include <QGraphicsPixmapItem>

enum collision_dir {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    NONE
};

class CollisionDirection
{

public:
    //Collision direction check on different axes
    collision_dir GetCollisionDirX(QGraphicsPixmapItem *character, QGraphicsPixmapItem *item);
    bool GetCollisionDown(QGraphicsPixmapItem *character, QGraphicsPixmapItem *item, float fallSpeed);
    bool GetCollisionUp(QGraphicsPixmapItem *character, QGraphicsPixmapItem *item, float jumpSpeed);
};

#endif // COLLISIONDIRECTION_H
