#include "consumable.h"

/*Constructor for a mask, syring or a star*/
Consumable::Consumable(typeConsumable type)
{
    //Initialization of the type of the consumable
    this->type = type;

    //Ressources image Seringue
    if (type == SYRINGE){
        QString syringe_image = ":/levels/ressources/seringue_Covid.png";
        syringe = QPixmap(syringe_image);

        //Initialization of the pixmap
        setPixmap(syringe);
    }

    //Ressources image Mask
    if (type == MASK) {
        QString mask_image = ":/levels/ressources/mask.png";
        mask = QPixmap(mask_image);

        //Initialization of the pixmap
        setPixmap(mask);
    }

    //Ressources image Star
    if (type == STAR) {
        QString star_image = ":/levels/ressources/star.png";
        star = QPixmap(star_image);

        //Initialization of the pixmap
        setPixmap(star);
    }
}

/*Method that animate the */
void Consumable::animateMask()
{
    if (type == MASK){
        animatedState++;
    }
}

/*Getters and setters*/
typeConsumable Consumable::getTypeConsumable(){
    return this->type;
}

int Consumable::getAnimatedState(){
    return animatedState;
}
