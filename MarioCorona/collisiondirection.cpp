#include "collisiondirection.h"

//Get the collision direction on the x axis
collision_dir CollisionDirection::GetCollisionDirX(QGraphicsPixmapItem *character, QGraphicsPixmapItem *item)
{
    //Compare the coordinate of the character and the given item
    if(character->y() + character->boundingRect().height() >= item->y() + 5){
        if (character->x() <= item->x()){
            return RIGHT;
        }
        else {
            return LEFT;
        }
    }
    else{
        return NONE;
    }
}

//Check if the collision, looking from the character, is coming from bellow
bool CollisionDirection::GetCollisionDown(QGraphicsPixmapItem *character, QGraphicsPixmapItem *item, float fallSpeed)
{
    if(character->y() + character->boundingRect().size().height() <= item->y() + fallSpeed){
        //Check if the character is not precisly on the side of the block
        if(character->x() + 2 < item->x() + item->boundingRect().size().width() &&
                character->x() + character->boundingRect().size().width() > item->x() + 2)
            return true;
    }
    return false;
}

//Check if the collision, looking from the character, is coming from above
bool CollisionDirection::GetCollisionUp(QGraphicsPixmapItem *character, QGraphicsPixmapItem *item, float jumpSpeed)
{
    if(character->y() + jumpSpeed >= item->y() + item->boundingRect().size().height()){
        return true;
    }

    return false;
}
