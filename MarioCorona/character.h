#ifndef CHARACTER_H
#define CHARACTER_H

#include <QGraphicsPixmapItem>

enum marioState {
    NORMAL,
    SMALL,
    DEAD,
    INVINCIBLE
};

class Character: public QGraphicsPixmapItem
{
public:
    Character();

    //Updates function
    void moveCharacter();
    void changeTexture();

    //Getters and setters on the state of the character
    virtual bool isWalkingRight() {return right;}
    virtual bool isWalkingLeft() {return left;}
    virtual void setRight(bool right) { this->right = right ;}
    virtual bool getRight() {return this->right;}
    virtual void setLeft(bool left) { this->left = left;}
    virtual bool getLeft() {return this->left;}
    virtual float getFallSpeed() {return temp_fall_speed + fall_decrease_speed;}
    virtual float getJumpSpeed() {return temp_jump_speed + jump_descrease_speed;}
    virtual void setFall(bool fall) {
        if(fall == true){
            this->fall = fall;
            this->temp_fall_speed = fall_speed;
        }
        else{
            this->fall = fall;
        }
    }
    virtual void setJumping(bool jump)
    {
        if(jump == true){
            this->jump = jump;
            this->temp_jump_speed = jump_speed;
        }
        else{
            this->jump = jump;
        }
    }
    virtual void setRunning(bool run)
    {
        this->run = run;
    }

    virtual bool isJumping() {return jump;}
    virtual bool isFalling() {return fall;}
    virtual bool isRunning() {return run;}

    //Check the current direction of mario
    bool isLookingRight();
    bool isLookingLeft();

    //State
    marioState getState() {return this->state;}
    void setState(marioState state) {this->state = state;}

    //Life management
    virtual int getLifes() {return lifes;}
    virtual void loseLife() {lifes -= 1;}
    virtual void gainLife() {lifes += 1;}

    //Textures
    //Mario Normal
    QPixmap getMarioRight() {return this->mario_right;}
    QPixmap getMarioLeft() {return this->mario_left;}
    QPixmap getMarioWalkingRight1() {return this->mario_right_walk1;}
    QPixmap getMarioWalkingRight2() {return this->mario_right_walk2;}
    QPixmap getMarioWalkingRight3() {return this->mario_right_walk3;}
    QPixmap getMarioWalkingLeft1() {return this->mario_left_walk1;}
    QPixmap getMarioWalkingLeft2() {return this->mario_left_walk2;}
    QPixmap getMarioWalkingLeft3() {return this->mario_left_walk3;}
    QPixmap getMarioJumpLeft() {return this->mario_jump_left;}
    QPixmap getMarioJumpRight() {return this->mario_jump_right;}

    //Mario Small
    QPixmap getMarioSmallRight() {return this->mario_small_right;}
    QPixmap getMarioSmallLeft() {return this->mario_small_left;}
    QPixmap getMarioSmallWalkingRight1() {return this->mario_small_right_walk1;}
    QPixmap getMarioSmallWalkingRight2() {return this->mario_small_right_walk2;}
    QPixmap getMarioSmallWalkingRight3() {return this->mario_small_right_walk3;}
    QPixmap getMarioSmallWalkingLeft1() {return this->mario_small_left_walk1;}
    QPixmap getMarioSmallWalkingLeft2() {return this->mario_small_left_walk2;}
    QPixmap getMarioSmallWalkingLeft3() {return this->mario_small_left_walk3;}
    QPixmap getMarioSmallJumpLeft() {return this->mario_small_jump_left;}
    QPixmap getMarioSmallJumpRight() {return this->mario_small_jump_right;}
    QPixmap getMarioDead() {return this->mario_dead;}

    //Hit function
    void hitMonster();
    bool getHitMonster() {return this->hit_monster;}
    void setHitMonster(bool hit) {this->hit_monster = hit;}
    void jumpMonster();
    bool getJumpMonster() { return this->jump_monster;}
    void setJumpMonster(bool jump_monster){ this->jump_monster = jump_monster; }


    //Invincible function
    void invincible();

private:
    bool right = false;
    bool left = false;
    bool fall = true;
    bool jump = false;
    bool run = false;
    marioState state;

    //Hit attributes
    bool hit_monster;
    int hit_time = 0;
    bool opacity = false;
    bool jump_monster = false;
    int jump_time = 0;

    //Life attribute
    int lifes = 4;

    //Walking speed
    float walk_range = 2;
    float run_walk_range = 3;
    float walk_range_jumping = 1.5;
    float walk_range_runing_jump_fall = 2;
    int walking_state = 0;

    //Fall speed
    float fall_speed = 2;
    float fall_decrease_speed = 5;
    float temp_fall_speed;

    //Jump speed
    float jump_speed = 5.6;
    float jump_descrease_speed = 0.1;
    float temp_jump_speed;

    //Mario normal textures
    QPixmap mario_right;
    QPixmap mario_left;
    QPixmap mario_jump_left;
    QPixmap mario_jump_right;
    QPixmap mario_right_walk1;
    QPixmap mario_right_walk2;
    QPixmap mario_right_walk3;
    QPixmap mario_left_walk1;
    QPixmap mario_left_walk2;
    QPixmap mario_left_walk3;

    //Mario small textures
    QPixmap mario_small_right;
    QPixmap mario_small_left;
    QPixmap mario_small_jump_left;
    QPixmap mario_small_jump_right;
    QPixmap mario_small_right_walk1;
    QPixmap mario_small_right_walk2;
    QPixmap mario_small_right_walk3;
    QPixmap mario_small_left_walk1;
    QPixmap mario_small_left_walk2;
    QPixmap mario_small_left_walk3;
    QPixmap mario_dead;
};

#endif // CHARACTER_H
