#ifndef LIFE_H
#define LIFE_H

#include <QGraphicsPixmapItem>
#include <QFont>
#include <QPainter>

class Life : public QGraphicsPixmapItem
{
public:
    Life(QGraphicsItem * parent=0);

    //Getter and setter on the lifes attributes
    void loseLife();
    void gainLife();
    int getLifes() {return life; }
    void setLifes(int number_life) {life = number_life; }

    //Set the design of the heart to pangolin when used for the boss
    void setPangolinHeart();

private:
    int life;

    //Add the pixmap of the heart to the current object
    void drawLifes();
};

#endif // LIFE_H
