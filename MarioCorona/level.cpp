#include "level.h"

Level::Level(QGraphicsScene *scene, int level)
{
    //Initialisation of landscape and clouds for each level
    landscape = new Landscape();
    clouds = landscape->createClouds();
    clouds->setPos(10, -300);
    scene->addItem(clouds);

    //Generate the level in the scene depending on the current level
    switch (level) {
        case 1:
            //Generate the landscape
            hillBush1 = landscape->createBushHill1();
            hillBush1->setPos(10, 0);
            scene->addItem(hillBush1);

            //Generate the ground
            ground_1 = groundMaker(3500, 48);
            ground_1->setPos(5, 32);
            scene->addItem(ground_1);

            //Creation of items for the level
            createBoxesList_1(scene);
            createListObjects_1(scene);
            createListMonsters_1(scene);

            //Hospital
            hospital = hospitalEnd();
            hospital->setX(3304);
            hospital->setY(-36);
            scene->addItem(hospital);
            break;

        case 2:
            //Generate the grounds of the level
            ground_2_1 = groundMaker(112, 48);
            ground_2_1->setPos(5, 32);
            ground_2_2 = groundMaker(112, 48);
            ground_2_2->setPos(3350, 32);
            scene->addItem(ground_2_1);
            scene->addItem(ground_2_2);

            //Creation of items for the level
            createBoxesList_2(scene);
            createListObjects_2(scene);
            createListMonsters_2(scene);

            //Hospital
            hospital = hospitalEnd();
            hospital->setX(3370);
            hospital->setY(-36);
            scene->addItem(hospital);
            break;

        case 3:
            //Generate the landscape
            hillBush_end = landscape->createBushHillEnd();
            hillBush_end->setPos(10, 0);
            scene->addItem(hillBush_end);

            //Generate the ground of end level
            ground_end = groundMaker(729, 48);
            ground_end->setPos(200, 32);
            scene->addItem(ground_end);

            //Creation of items for the level
            createListMonsters_end(scene);
            createBoxesList_end(scene);
            createListObjects_end(scene);
    }

    //Initialize consumable list
    listConsumable = new QList<Consumable*>();
}

//Function to create a ground with specific lenght and width
QGraphicsPixmapItem* Level::groundMaker(int length, int width) {
    //Texture
    QString wall = ":/levels/ressources/wall.png";
    QPixmap collage(length, width);
    QPainter painter(&collage);
    QPixmap single_block(wall);

    //Adding each wall to the painter
    for(int y=0; y<width; y += single_block.height())
        for(int x=0; x<length; x += single_block.width())
            painter.drawPixmap(x, y, single_block);

    return new QGraphicsPixmapItem(collage);
}

//Function to create stairs
QGraphicsPixmapItem* Level::stair(int num_width) {
    //Texture
    QString block = ":/levels/ressources/block.png";
    QPixmap single_block(block);
    QPixmap collage(single_block.width()*num_width, single_block.height());
    QPainter painter(&collage);

    //Adding each stair to the painter
    for(int x=0; x < single_block.width()*num_width; x += single_block.width()){
        painter.drawPixmap(x, 0, single_block);
    }

    return new QGraphicsPixmapItem(collage);
}

//Function to create a small pipe
QGraphicsPixmapItem* Level::pipeSmall() {
    //Texture
    QString pipe_small = ":/levels/ressources/pipe-small.png";
    QPixmap collage(pipe_small);

    return new QGraphicsPixmapItem(collage);
}

//Function to create a medium pipe
QGraphicsPixmapItem* Level::pipeMedium() {
    //Texture
    QString pipe_medium = ":/levels/ressources/pipe-med.png";
    QPixmap collage(pipe_medium);

    return new QGraphicsPixmapItem(collage);
}

//Function to create a big pipe
QGraphicsPixmapItem* Level::pipeBig() {
    //Texture
    QString pipe_big = ":/levels/ressources/pipe-big.png";
    QPixmap collage(pipe_big);

    return new QGraphicsPixmapItem(collage);
}

//Function to create the hospital for the end of level 1 and 2
QGraphicsPixmapItem* Level::hospitalEnd() {
    //Texture
    QString hospital = ":/levels/ressources/hopital.png";
    QPixmap collage(hospital);

    return new QGraphicsPixmapItem(collage);
}

//Function to create an invisible wall
QGraphicsPixmapItem* Level::invisibleWall() {
    //Creation of the pixmap with opacity at 0
    QPixmap collage(16, 200);
    QGraphicsPixmapItem *result = new QGraphicsPixmapItem(collage);
    result->setOpacity(0);

    return result;
}

//Function to create the list of boxes and bricks for level 1
void Level::createBoxesList_1(QGraphicsScene *scene)
{
    listBoxes_1 = new QList<Boxes*>();

    /*Bricks*/
    Boxes *brick1 = new Boxes(BRICK, 3);
    Boxes *brick2 = new Boxes(BRICK, 2);
    Boxes *brick3 = new Boxes(BRICK);
    Boxes *brick4 = new Boxes(BRICK, 5);
    Boxes *brick5 = new Boxes(BRICK, 1);
    Boxes *brick6 = new Boxes(BRICK, 2);
    Boxes *brick7 = new Boxes(BRICK, 2);
    Boxes *brick8 = new Boxes(BRICK);
    Boxes *brick9 = new Boxes(BRICK);
    Boxes *brick10 = new Boxes(BRICK, 2);
    Boxes *brick11 = new Boxes(BRICK, 3);
    Boxes *brick12 = new Boxes(BRICK);
    Boxes *brick13 = new Boxes(BRICK);
    Boxes *brick14 = new Boxes(BRICK);
    Boxes *brick15 = new Boxes(BRICK, 2);
    Boxes *brick16 = new Boxes(BRICK);
    Boxes *brick17 = new Boxes(BRICK, 3);
    Boxes *brick18 = new Boxes(BRICK, 3);
    Boxes *brick19 = new Boxes(BRICK, 3);
    Boxes *brick20 = new Boxes(BRICK);
    Boxes *brick21 = new Boxes(BRICK);
    Boxes *brick22 = new Boxes(BRICK);
    Boxes *brick23 = new Boxes(BRICK, 2);
    Boxes *brick24 = new Boxes(BRICK, 2);
    Boxes *brick25 = new Boxes(BRICK, 2);
    Boxes *brick26 = new Boxes(BRICK, 2);
    Boxes *brick27 = new Boxes(BRICK, 2);
    Boxes *brick28 = new Boxes(BRICK, 2);

    //Setting the position of each brick
    brick1->setX(284);
    brick1->setY(-27);
    scene->addItem(brick1);

    brick2->setX(316);
    brick2->setY(-27);
    scene->addItem(brick2);

    brick10->setX(350);
    brick10->setY(-50);
    scene->addItem(brick10);

    brick11->setX(366);
    brick11->setY(-50);
    scene->addItem(brick11);

    brick12->setX(382);
    brick12->setY(-50);
    scene->addItem(brick12);

    brick13->setX(398);
    brick13->setY(-50);
    scene->addItem(brick13);

    brick3->setX(774);
    brick3->setY(-50);
    scene->addItem(brick3);

    brick4->setX(806);
    brick4->setY(-50);
    scene->addItem(brick4);

    brick5->setX(822);
    brick5->setY(-50);
    scene->addItem(brick5);

    brick6->setX(838);
    brick6->setY(-50);
    scene->addItem(brick6);

    brick14->setX(930);
    brick14->setY(-80);
    scene->addItem(brick14);

    brick15->setX(946);
    brick15->setY(-80);
    scene->addItem(brick15);

    brick16->setX(962);
    brick16->setY(-80);
    scene->addItem(brick16);

    brick17->setX(1030);
    brick17->setY(-80);
    scene->addItem(brick17);

    brick18->setX(1046);
    brick18->setY(-80);
    scene->addItem(brick18);

    brick19->setX(1062);
    brick19->setY(-80);
    scene->addItem(brick19);

    brick7->setX(1284);
    brick7->setY(-50);
    scene->addItem(brick7);

    brick8->setX(1316);
    brick8->setY(-50);
    scene->addItem(brick8);

    brick9->setX(1332);
    brick9->setY(-50);
    scene->addItem(brick9);

    brick20->setX(1630);
    brick20->setY(-28);
    scene->addItem(brick20);

    brick21->setX(1700);
    brick21->setY(-28);
    scene->addItem(brick21);

    brick22->setX(1770);
    brick22->setY(-28);
    scene->addItem(brick22);

    brick23->setX(2230);
    brick23->setY(-70);
    scene->addItem(brick23);

    brick24->setX(2246);
    brick24->setY(-70);
    scene->addItem(brick24);

    brick25->setX(2262);
    brick25->setY(-70);
    scene->addItem(brick25);

    brick26->setX(2278);
    brick26->setY(-70);
    scene->addItem(brick26);

    brick27->setX(2294);
    brick27->setY(-70);
    scene->addItem(brick27);

    brick28->setX(2310);
    brick28->setY(-70);
    scene->addItem(brick28);

    //Boxes
    Boxes *box0 = new Boxes(BOX);
    Boxes *box1 = new Boxes(BOX);
    Boxes *box2 = new Boxes(BOX);
    Boxes *box3 = new Boxes(BOX);
    Boxes *box4 = new Boxes(BOX);
    Boxes *box5 = new Boxes(BOX);
    Boxes *box6 = new Boxes(BOX);

    //Setting the position of boxes
    box0->setX(300);
    box0->setY(-27);
    scene->addItem(box0);

    box1->setX(790);
    box1->setY(-50);
    scene->addItem(box1);

    box2->setX(1300);
    box2->setY(-50);
    scene->addItem(box2);

    box3->setX(2000);
    box3->setY(-50);
    scene->addItem(box3);

    box4->setX(1700);
    box4->setY(-80);
    scene->addItem(box4);

    box5->setX(2254);
    box5->setY(-130);
    scene->addItem(box5);

    box6->setX(2286);
    box6->setY(-130);
    scene->addItem(box6);

    //Add the bricks and boxes to the list
    listBoxes_1->push_back(brick1);
    listBoxes_1->push_back(brick2);
    listBoxes_1->push_back(brick3);
    listBoxes_1->push_back(brick4);
    listBoxes_1->push_back(brick5);
    listBoxes_1->push_back(brick6);
    listBoxes_1->push_back(brick7);
    listBoxes_1->push_back(brick8);
    listBoxes_1->push_back(brick9);
    listBoxes_1->push_back(brick10);
    listBoxes_1->push_back(brick11);
    listBoxes_1->push_back(brick12);
    listBoxes_1->push_back(brick13);
    listBoxes_1->push_back(brick14);
    listBoxes_1->push_back(brick15);
    listBoxes_1->push_back(brick16);
    listBoxes_1->push_back(brick17);
    listBoxes_1->push_back(brick18);
    listBoxes_1->push_back(brick19);
    listBoxes_1->push_back(brick20);
    listBoxes_1->push_back(brick21);
    listBoxes_1->push_back(brick22);
    listBoxes_1->push_back(brick23);
    listBoxes_1->push_back(brick24);
    listBoxes_1->push_back(brick25);
    listBoxes_1->push_back(brick26);
    listBoxes_1->push_back(brick27);
    listBoxes_1->push_back(brick28);

    listBoxes_1->push_back(box0);
    listBoxes_1->push_back(box1);
    listBoxes_1->push_back(box2);
    listBoxes_1->push_back(box3);
    listBoxes_1->push_back(box4);
    listBoxes_1->push_back(box5);
    listBoxes_1->push_back(box6);
}

//Function to create the list of objects for level 1
void Level::createListObjects_1(QGraphicsScene *scene)
{
    //Invisible wall to avoid mario left direction at the start of the level
    QGraphicsPixmapItem *invisible_wall = invisibleWall();
    invisible_wall->setPos(0,-170);
    scene->addItem(invisible_wall);

    /* Creation of the objects */
    QGraphicsPixmapItem *pipe1;
    QGraphicsPixmapItem *pipe2;
    QGraphicsPixmapItem *pipe3;
    QGraphicsPixmapItem *pipe4;
    QGraphicsPixmapItem *pipe5;
    QGraphicsPixmapItem *pipe6;
    QGraphicsPixmapItem *pipe7;

    QGraphicsPixmapItem *upstair1;
    QGraphicsPixmapItem *upstair2;
    QGraphicsPixmapItem *upstair3;
    QGraphicsPixmapItem *upstair4;
    QGraphicsPixmapItem *upstair5;
    QGraphicsPixmapItem *upstair6;
    QGraphicsPixmapItem *upstair7;
    QGraphicsPixmapItem *upstair8;
    QGraphicsPixmapItem *upstair9;
    QGraphicsPixmapItem *upstair10;
    QGraphicsPixmapItem *upstair11;
    QGraphicsPixmapItem *upstair12;
    QGraphicsPixmapItem *upstair13;
    QGraphicsPixmapItem *upstair14;
    QGraphicsPixmapItem *upstair15;
    QGraphicsPixmapItem *upstair16;
    QGraphicsPixmapItem *upstair17;
    QGraphicsPixmapItem *upstair18;
    QGraphicsPixmapItem *upstair19;
    QGraphicsPixmapItem *upstair20;
    QGraphicsPixmapItem *upstair21;
    QGraphicsPixmapItem *upstair22;
    QGraphicsPixmapItem *upstair23;
    QGraphicsPixmapItem *upstair24;
    QGraphicsPixmapItem *upstair25;
    QGraphicsPixmapItem *upstair26;
    QGraphicsPixmapItem *upstair27;
    QGraphicsPixmapItem *upstair28;
    QGraphicsPixmapItem *upstair29;
    QGraphicsPixmapItem *upstair30;
    QGraphicsPixmapItem *upstair31;
    QGraphicsPixmapItem *upstair32;
    QGraphicsPixmapItem *upstair33;
    QGraphicsPixmapItem *upstair34;
    QGraphicsPixmapItem *upstair35;
    QGraphicsPixmapItem *upstair36;
    QGraphicsPixmapItem *upstair37;
    QGraphicsPixmapItem *upstair38;
    QGraphicsPixmapItem *upstair39;
    QGraphicsPixmapItem *upstair40;
    QGraphicsPixmapItem *upstair41;
    QGraphicsPixmapItem *upstair42;
    QGraphicsPixmapItem *upstair43;
    QGraphicsPixmapItem *upstair44;
    QGraphicsPixmapItem *upstair45;
    QGraphicsPixmapItem *upstair46;
    QGraphicsPixmapItem *upstair47;
    QGraphicsPixmapItem *upstair48;
    QGraphicsPixmapItem *upstair49;
    QGraphicsPixmapItem *upstair50;
    QGraphicsPixmapItem *upstair51;
    QGraphicsPixmapItem *upstair52;
    QGraphicsPixmapItem *upstair53;
    QGraphicsPixmapItem *upstair54;
    QGraphicsPixmapItem *upstair55;
    QGraphicsPixmapItem *upstair56;
    QGraphicsPixmapItem *upstair57;
    QGraphicsPixmapItem *upstair58;
    /* End of the creation */

    /*Pipes of the level*/
    pipe1 = pipeSmall();
    pipe1->setX(500);
    pipe1->setY(0);
    scene->addItem(pipe1);

    pipe2 = pipeMedium();
    pipe2->setX(580);
    pipe2->setY(-16);
    scene->addItem(pipe2);

    pipe3 = pipeBig();
    pipe3->setX(660);
    pipe3->setY(-32);
    scene->addItem(pipe3);

    pipe7 = pipeMedium();
    pipe7->setX(930);
    pipe7->setY(-16);
    scene->addItem(pipe7);

    pipe4 = pipeBig();
    pipe4->setX(1200);
    pipe4->setY(-32);
    scene->addItem(pipe4);

    pipe5 = pipeBig();
    pipe5->setX(1900);
    pipe5->setY(-32);
    scene->addItem(pipe5);

    pipe6 = pipeSmall();
    pipe6->setX(2500);
    pipe6->setY(0);
    scene->addItem(pipe6);
    /*End of pipes*/

    /*Upstairs*/
    upstair1 = stair(4);
    upstair1->setX(2100);
    upstair1->setY(16);
    scene->addItem(upstair1);

    upstair2 = stair(3);
    upstair2->setX(2116);
    upstair2->setY(0);
    scene->addItem(upstair2);

    upstair3 = stair(2);
    upstair3->setX(2132);
    upstair3->setY(-16);
    scene->addItem(upstair3);

    upstair4 = stair(1);
    upstair4->setX(2148);
    upstair4->setY(-32);
    scene->addItem(upstair4);

    upstair5 = stair(16);
    upstair5->setX(2408);
    upstair5->setY(16);
    scene->addItem(upstair5);

    upstair6 = stair(15);
    upstair6->setX(2424);
    upstair6->setY(0);
    scene->addItem(upstair6);

    upstair7 = stair(14);
    upstair7->setX(2440);
    upstair7->setY(-16);
    scene->addItem(upstair7);

    upstair8 = stair(13);
    upstair8->setX(2456);
    upstair8->setY(-32);
    scene->addItem(upstair8);

    upstair9 = stair(12);
    upstair9->setX(2472);
    upstair9->setY(-48);
    scene->addItem(upstair9);

    upstair10 = stair(11);
    upstair10->setX(2488);
    upstair10->setY(-64);
    scene->addItem(upstair10);

    upstair11 = stair(10);
    upstair11->setX(2504);
    upstair11->setY(-80);
    scene->addItem(upstair11);

    upstair12 = stair(9);
    upstair12->setX(2520);
    upstair12->setY(-96);
    scene->addItem(upstair12);

    upstair13 = stair(8);
    upstair13->setX(2536);
    upstair13->setY(-112);
    scene->addItem(upstair13);

    upstair14 = stair(7);
    upstair14->setX(2552);
    upstair14->setY(-128);
    scene->addItem(upstair14);

    upstair15 = stair(6);
    upstair15->setX(2568);
    upstair15->setY(-144);
    scene->addItem(upstair15);

    upstair16 = stair(5);
    upstair16->setX(2584);
    upstair16->setY(-160);
    scene->addItem(upstair16);

    upstair17 = stair(4);
    upstair17->setX(2600);
    upstair17->setY(-176);
    scene->addItem(upstair17);

    upstair18 = stair(3);
    upstair18->setX(2616);
    upstair18->setY(-192);
    scene->addItem(upstair18);

    upstair19 = stair(2);
    upstair19->setX(2632);
    upstair19->setY(-208);
    scene->addItem(upstair19);

    upstair20 = stair(1);
    upstair20->setX(2648);
    upstair20->setY(-224);
    scene->addItem(upstair20);

    upstair21 = stair(1);
    upstair21->setX(2664);
    upstair21->setY(-208);
    scene->addItem(upstair21);

    upstair22 = stair(2);
    upstair22->setX(2664);
    upstair22->setY(-192);
    scene->addItem(upstair22);

    upstair23 = stair(41);
    upstair23->setX(2664);
    upstair23->setY(-176);
    scene->addItem(upstair23);

    upstair56 = stair(1);
    upstair56->setX(3272);
    upstair56->setY(-192);
    scene->addItem(upstair56);

    upstair24 = stair(2);
    upstair24->setX(3288);
    upstair24->setY(-192);
    scene->addItem(upstair24);

    upstair25 = stair(1);
    upstair25->setX(3304);
    upstair25->setY(-208);
    scene->addItem(upstair25);

    upstair26 = stair(1);
    upstair26->setX(3320);
    upstair26->setY(-48);
    scene->addItem(upstair26);

    upstair57 = stair(1);
    upstair57->setX(3320);
    upstair57->setY(-64);
    scene->addItem(upstair57);

    upstair27 = stair(1);
    upstair27->setX(3320);
    upstair27->setY(-80);
    scene->addItem(upstair27);

    upstair28 = stair(1);
    upstair28->setX(3320);
    upstair28->setY(-96);
    scene->addItem(upstair28);

    upstair29 = stair(1);
    upstair29->setX(3320);
    upstair29->setY(-112);
    scene->addItem(upstair29);

    upstair30 = stair(1);
    upstair30->setX(3320);
    upstair30->setY(-128);
    scene->addItem(upstair30);

    upstair31 = stair(1);
    upstair31->setX(3320);
    upstair31->setY(-144);
    scene->addItem(upstair31);

    upstair32 = stair(1);
    upstair32->setX(3320);
    upstair32->setY(-160);
    scene->addItem(upstair32);

    upstair33 = stair(1);
    upstair33->setX(3320);
    upstair33->setY(-176);
    scene->addItem(upstair33);

    upstair34 = stair(1);
    upstair34->setX(3320);
    upstair34->setY(-192);
    scene->addItem(upstair34);

    upstair35 = stair(1);
    upstair35->setX(3320);
    upstair35->setY(-208);
    scene->addItem(upstair35);

    upstair36 = stair(1);
    upstair36->setX(3352);
    upstair36->setY(-48);
    scene->addItem(upstair36);

    upstair37 = stair(1);
    upstair37->setX(3352);
    upstair37->setY(-64);
    scene->addItem(upstair37);

    upstair58 = stair(1);
    upstair58->setX(3352);
    upstair58->setY(-80);
    scene->addItem(upstair58);

    upstair38 = stair(1);
    upstair38->setX(3352);
    upstair38->setY(-96);
    scene->addItem(upstair38);

    upstair39 = stair(1);
    upstair39->setX(3352);
    upstair39->setY(-112);
    scene->addItem(upstair39);

    upstair40 = stair(1);
    upstair40->setX(3352);
    upstair40->setY(-128);
    scene->addItem(upstair40);

    upstair41 = stair(1);
    upstair41->setX(3352);
    upstair41->setY(-144);
    scene->addItem(upstair41);

    upstair42 = stair(1);
    upstair42->setX(3352);
    upstair42->setY(-160);
    scene->addItem(upstair42);

    upstair43 = stair(1);
    upstair43->setX(3352);
    upstair43->setY(-176);
    scene->addItem(upstair43);

    upstair44 = stair(1);
    upstair44->setX(3352);
    upstair44->setY(-192);
    scene->addItem(upstair44);

    upstair45 = stair(1);
    upstair45->setX(3352);
    upstair45->setY(-208);
    scene->addItem(upstair45);

    upstair46 = stair(1);
    upstair46->setX(2744);
    upstair46->setY(-192);
    scene->addItem(upstair46);

    upstair47 = stair(1);
    upstair47->setX(2808);
    upstair47->setY(-192);
    scene->addItem(upstair47);

    upstair48 = stair(1);
    upstair48->setX(2872);
    upstair48->setY(-192);
    scene->addItem(upstair48);

    upstair49 = stair(1);
    upstair49->setX(2936);
    upstair49->setY(-192);
    scene->addItem(upstair49);

    upstair50 = stair(1);
    upstair50->setX(3000);
    upstair50->setY(-192);
    scene->addItem(upstair50);

    upstair51 = stair(1);
    upstair51->setX(3064);
    upstair51->setY(-192);
    scene->addItem(upstair51);

    upstair52 = stair(1);
    upstair52->setX(3128);
    upstair52->setY(-192);
    scene->addItem(upstair52);

    upstair53 = stair(1);
    upstair53->setX(3192);
    upstair53->setY(-192);
    scene->addItem(upstair53);

    upstair54 = stair(1);
    upstair54->setX(3256);
    upstair54->setY(-192);
    scene->addItem(upstair54);

    upstair55 = stair(1);
    upstair55->setX(3320);
    upstair55->setY(-192);
    scene->addItem(upstair55);
    /*End of upstairs*/

    /*Put objects of the scene in a list*/
    listObjects_1 = new QList<QGraphicsPixmapItem*>();
    listObjects_1->push_back(pipe1);
    listObjects_1->push_back(pipe2);
    listObjects_1->push_back(pipe3);
    listObjects_1->push_back(pipe4);
    listObjects_1->push_back(pipe5);
    listObjects_1->push_back(pipe6);
    listObjects_1->push_back(pipe7);

    listObjects_1->push_back(upstair1);
    listObjects_1->push_back(upstair2);
    listObjects_1->push_back(upstair3);
    listObjects_1->push_back(upstair4);
    listObjects_1->push_back(upstair5);
    listObjects_1->push_back(upstair6);
    listObjects_1->push_back(upstair7);
    listObjects_1->push_back(upstair8);
    listObjects_1->push_back(upstair9);
    listObjects_1->push_back(upstair10);
    listObjects_1->push_back(upstair11);
    listObjects_1->push_back(upstair12);
    listObjects_1->push_back(upstair13);
    listObjects_1->push_back(upstair14);
    listObjects_1->push_back(upstair15);
    listObjects_1->push_back(upstair16);
    listObjects_1->push_back(upstair17);
    listObjects_1->push_back(upstair18);
    listObjects_1->push_back(upstair19);
    listObjects_1->push_back(upstair20);
    listObjects_1->push_back(upstair21);
    listObjects_1->push_back(upstair22);
    listObjects_1->push_back(upstair23);
    listObjects_1->push_back(upstair24);
    listObjects_1->push_back(upstair25);
    listObjects_1->push_back(upstair26);
    listObjects_1->push_back(upstair27);
    listObjects_1->push_back(upstair28);
    listObjects_1->push_back(upstair29);
    listObjects_1->push_back(upstair30);
    listObjects_1->push_back(upstair31);
    listObjects_1->push_back(upstair32);
    listObjects_1->push_back(upstair33);
    listObjects_1->push_back(upstair34);
    listObjects_1->push_back(upstair35);
    listObjects_1->push_back(upstair36);
    listObjects_1->push_back(upstair37);
    listObjects_1->push_back(upstair38);
    listObjects_1->push_back(upstair39);
    listObjects_1->push_back(upstair40);
    listObjects_1->push_back(upstair41);
    listObjects_1->push_back(upstair42);
    listObjects_1->push_back(upstair43);
    listObjects_1->push_back(upstair44);
    listObjects_1->push_back(upstair45);
    listObjects_1->push_back(upstair46);
    listObjects_1->push_back(upstair47);
    listObjects_1->push_back(upstair48);
    listObjects_1->push_back(upstair49);
    listObjects_1->push_back(upstair50);
    listObjects_1->push_back(upstair51);
    listObjects_1->push_back(upstair52);
    listObjects_1->push_back(upstair53);
    listObjects_1->push_back(upstair54);
    listObjects_1->push_back(upstair55);
    listObjects_1->push_back(upstair56);
    listObjects_1->push_back(upstair57);
    listObjects_1->push_back(upstair58);
    listObjects_1->push_back(invisible_wall);
    /*End of the list*/
}

//Function to create the list of monsters for level 1
void Level::createListMonsters_1(QGraphicsScene *scene)
{
    //Monsters
    Monster *covid1 = new Monster(COVID);
    Monster *covid2 = new Monster(COVID);
    Monster *covid3 = new Monster(COVID);
    Monster *covid4 = new Monster(COVID);
    Monster *covid5 = new Monster(COVID);
    Monster *covid7 = new Monster(COVID);
    Monster *covid8 = new Monster(COVID);
    Monster *covid9 = new Monster(COVID);
    Monster *covid10 = new Monster(COVID);
    Monster *covid11 = new Monster(COVID);
    Monster *covid12 = new Monster(COVID);
    Monster *covid13 = new Monster(COVID);
    Monster *covid14 = new Monster(COVID);
    Monster *covid15 = new Monster(COVID);
    Monster *covid16 = new Monster(COVID);
    Monster *covid17 = new Monster(COVID);
    Monster *covid18 = new Monster(COVID);
    Monster *covid19 = new Monster(COVID);
    Monster *covid20 = new Monster(COVID);
    Monster *covid21 = new Monster(COVID);

    //Position of each monster
    covid1->setX(540);
    covid1->setY(16);
    scene->addItem(covid1);

    covid2->setX(630);
    covid2->setY(16);
    scene->addItem(covid2);

    covid3->setX(1400);
    covid3->setY(16);
    scene->addItem(covid3);

    covid4->setX(727);
    covid4->setY(16);
    scene->addItem(covid4);

    covid5->setX(863);
    covid5->setY(16);
    scene->addItem(covid5);

    covid8->setX(997);
    covid8->setY(16);
    scene->addItem(covid8);

    covid9->setX(1133);
    covid9->setY(16);
    scene->addItem(covid9);

    covid7->setX(2100);
    covid7->setY(16);
    scene->addItem(covid7);

    covid10->setX(2234);
    covid10->setY(16);
    scene->addItem(covid10);

    covid11->setX(2320);
    covid11->setY(16);
    scene->addItem(covid11);

    covid12->setX(2406);
    covid12->setY(16);
    scene->addItem(covid12);

    covid13->setX(2712);
    covid13->setY(-192);
    scene->addItem(covid13);

    covid14->setX(2776);
    covid14->setY(-192);
    scene->addItem(covid14);

    covid16->setX(2840);
    covid16->setY(-192);
    scene->addItem(covid16);

    covid17->setX(2904);
    covid17->setY(-192);
    scene->addItem(covid17);

    covid18->setX(2968);
    covid18->setY(-192);
    scene->addItem(covid18);

    covid15->setX(3032);
    covid15->setY(-192);
    scene->addItem(covid15);

    covid19->setX(3096);
    covid19->setY(-192);
    scene->addItem(covid19);

    covid20->setX(3160);
    covid20->setY(-192);
    scene->addItem(covid20);

    covid21->setX(3224);
    covid21->setY(-192);
    scene->addItem(covid21);

    //Add each monster to the list
    listMonsters_1 = new QList<Monster*>();
    listMonsters_1->push_back(covid1);
    listMonsters_1->push_back(covid2);
    listMonsters_1->push_back(covid3);
    listMonsters_1->push_back(covid4);
    listMonsters_1->push_back(covid5);
    listMonsters_1->push_back(covid7);
    listMonsters_1->push_back(covid8);
    listMonsters_1->push_back(covid9);
    listMonsters_1->push_back(covid10);
    listMonsters_1->push_back(covid11);
    listMonsters_1->push_back(covid12);
    listMonsters_1->push_back(covid13);
    listMonsters_1->push_back(covid14);
    listMonsters_1->push_back(covid15);
    listMonsters_1->push_back(covid16);
    listMonsters_1->push_back(covid17);
    listMonsters_1->push_back(covid18);
    listMonsters_1->push_back(covid19);
    listMonsters_1->push_back(covid20);
    listMonsters_1->push_back(covid21);
}

//Function to create the list of boxes and bricks for level 2
void Level::createBoxesList_2(QGraphicsScene *scene) {
    this->listBoxes_2 = new QList<Boxes*>();

    /*Bricks*/
    Boxes *brick1 = new Boxes(BRICK, 3);
    Boxes *brick2 = new Boxes(BRICK, 3);
    Boxes *brick3 = new Boxes(BRICK, 4);
    Boxes *brick4 = new Boxes(BRICK, 4);

    //Set the position
    brick1->setPos(640, -285);
    scene->addItem(brick1);

    brick2->setPos(672, -285);
    scene->addItem(brick2);

    brick3->setPos(2926, -240);
    scene->addItem(brick3);

    brick4->setPos(2974, -240);
    scene->addItem(brick4);

    /*Add the bricks to the boxes*/
    listBoxes_2->push_back(brick1);
    listBoxes_2->push_back(brick2);
    listBoxes_2->push_back(brick3);
    listBoxes_2->push_back(brick4);

    /*Boxes*/
    Boxes *box1 = new Boxes(BOX);
    Boxes *box2 = new Boxes(BOX);
    Boxes *box3 = new Boxes(BOX);
    Boxes *box4 = new Boxes(BOX);

    //Set the position
    box1->setPos(70, -27);
    scene->addItem(box1);

    box2->setPos(656, -285);
    scene->addItem(box2);

    box3->setPos(2942, -240);
    scene->addItem(box3);

    box4->setPos(2958, -240);
    scene->addItem(box4);

    /*Add the boxes to the list*/
    listBoxes_2->push_back(box1);
    listBoxes_2->push_back(box2);
    listBoxes_2->push_back(box3);
    listBoxes_2->push_back(box4);
}

//Function to create the list of objects for level 2
void Level::createListObjects_2(QGraphicsScene *scene) {
    this->listObjects_2 = new QList<QGraphicsPixmapItem*>();

    //Invisible wall at the start of the level
    QGraphicsPixmapItem *invisible_wall = invisibleWall();
    invisible_wall->setPos(0,-170);
    scene->addItem(invisible_wall);

    /*Blocks*/
    QGraphicsPixmapItem *block1 = stair(1);
    QGraphicsPixmapItem *block2 = stair(1);
    QGraphicsPixmapItem *block3 = stair(1);
    QGraphicsPixmapItem *block4 = stair(8);
    QGraphicsPixmapItem *block5 = stair(1);
    QGraphicsPixmapItem *block6 = stair(1);
    QGraphicsPixmapItem *block7 = stair(13);
    QGraphicsPixmapItem *block8 = stair(1);
    QGraphicsPixmapItem *block9 = stair(1);
    QGraphicsPixmapItem *block10 = stair(12);
    QGraphicsPixmapItem *block11 = stair(1);
    QGraphicsPixmapItem *block12 = stair(1);
    QGraphicsPixmapItem *block13 = stair(1);
    QGraphicsPixmapItem *block14 = stair(1);
    QGraphicsPixmapItem *block15 = stair(1);
    QGraphicsPixmapItem *block16 = stair(1);
    QGraphicsPixmapItem *block17 = stair(8);
    QGraphicsPixmapItem *block18 = stair(1);
    QGraphicsPixmapItem *block19 = stair(65);
    QGraphicsPixmapItem *block20 = stair(68);
    QGraphicsPixmapItem *block21 = stair(2);
    QGraphicsPixmapItem *block22 = stair(1);
    QGraphicsPixmapItem *block23 = stair(1);
    QGraphicsPixmapItem *block24 = stair(1);
    QGraphicsPixmapItem *block25 = stair(1);
    QGraphicsPixmapItem *block26 = stair(1);
    QGraphicsPixmapItem *block27 = stair(1);
    QGraphicsPixmapItem *block28 = stair(1);
    QGraphicsPixmapItem *block29 = stair(1);
    QGraphicsPixmapItem *block30 = stair(1);
    QGraphicsPixmapItem *block31 = stair(1);
    QGraphicsPixmapItem *block32 = stair(1);
    QGraphicsPixmapItem *block33 = stair(1);
    QGraphicsPixmapItem *block34 = stair(1);
    QGraphicsPixmapItem *block35 = stair(1);
    QGraphicsPixmapItem *block36 = stair(1);
    QGraphicsPixmapItem *block37 = stair(1);
    QGraphicsPixmapItem *block38 = stair(1);
    QGraphicsPixmapItem *block39 = stair(1);
    QGraphicsPixmapItem *block40 = stair(1);
    QGraphicsPixmapItem *block41 = stair(1);
    QGraphicsPixmapItem *block42 = stair(2);
    QGraphicsPixmapItem *block43 = stair(2);
    QGraphicsPixmapItem *block44 = stair(2);
    QGraphicsPixmapItem *block45 = stair(2);
    QGraphicsPixmapItem *block46 = stair(4);
    QGraphicsPixmapItem *block47 = stair(4);
    QGraphicsPixmapItem *block48 = stair(3);
    QGraphicsPixmapItem *block49 = stair(2);
    QGraphicsPixmapItem *block50 = stair(1);
    QGraphicsPixmapItem *block51 = stair(3);
    QGraphicsPixmapItem *block52 = stair(2);
    QGraphicsPixmapItem *block53 = stair(1);
    QGraphicsPixmapItem *block54 = stair(12);
    QGraphicsPixmapItem *block55 = stair(1);
    QGraphicsPixmapItem *block56 = stair(1);

    //Set the position
    block1->setX(180);
    block1->setY(-30);
    scene->addItem(block1);

    block2->setX(260);
    block2->setY(-60);
    scene->addItem(block2);

    block3->setX(310);
    block3->setY(-120);
    scene->addItem(block3);

    block4->setX(360);
    block4->setY(-160);
    scene->addItem(block4);

    block5->setX(376);
    block5->setY(-176);
    scene->addItem(block5);

    block6->setX(456);
    block6->setY(-176);
    scene->addItem(block6);

    block7->setX(560);
    block7->setY(-200);
    scene->addItem(block7);

    block8->setX(576);
    block8->setY(-216);
    scene->addItem(block8);

    block9->setX(736);
    block9->setY(-216);
    scene->addItem(block9);

    block10->setX(310);
    block10->setY(-10);
    scene->addItem(block10);

    block11->setX(326);
    block11->setY(-26);
    scene->addItem(block11);

    block12->setX(470);
    block12->setY(-26);
    scene->addItem(block12);

    block13->setX(570);
    block13->setY(-26);
    scene->addItem(block13);

    block14->setX(650);
    block14->setY(-45);
    scene->addItem(block14);

    block15->setX(720);
    block15->setY(-26);
    scene->addItem(block15);

    block16->setX(800);
    block16->setY(-58);
    scene->addItem(block16);

    block17->setX(970);
    block17->setY(-30);
    scene->addItem(block17);

    block18->setX(850);
    block18->setY(-120);
    scene->addItem(block18);

    block19->setX(1098);
    block19->setY(-130);
    scene->addItem(block19);

    block20->setX(1098);
    block20->setY(-62);
    scene->addItem(block20);

    block21->setX(1066);
    block21->setY(-46);
    scene->addItem(block21);

    block22->setX(1082);
    block22->setY(-62);
    scene->addItem(block22);

    block23->setX(1098);
    block23->setY(-146);
    scene->addItem(block23);

    block24->setX(1162);
    block24->setY(-78);
    scene->addItem(block24);

    block36->setX(1242);
    block36->setY(-114);
    scene->addItem(block36);

    block30->setX(1322);
    block30->setY(-78);
    scene->addItem(block30);

    block37->setX(1402);
    block37->setY(-114);
    scene->addItem(block37);

    block25->setX(1482);
    block25->setY(-78);
    scene->addItem(block25);

    block38->setX(1562);
    block38->setY(-114);
    scene->addItem(block38);

    block31->setX(1642);
    block31->setY(-78);
    scene->addItem(block31);

    block39->setX(1722);
    block39->setY(-114);
    scene->addItem(block39);

    block26->setX(1802);
    block26->setY(-78);
    scene->addItem(block26);

    block40->setX(1882);
    block40->setY(-114);
    scene->addItem(block40);

    block32->setX(1962);
    block32->setY(-78);
    scene->addItem(block32);

    block41->setX(2042);
    block41->setY(-114);
    scene->addItem(block41);

    block27->setX(2122);
    block27->setY(-78);
    scene->addItem(block27);

    block28->setX(2122);
    block28->setY(-146);
    scene->addItem(block28);

    block42->setX(2300);
    block42->setY(-62);
    scene->addItem(block42);

    block43->setX(2400);
    block43->setY(-62);
    scene->addItem(block43);

    block44->setX(2500);
    block44->setY(-90);
    scene->addItem(block44);

    block45->setX(2625);
    block45->setY(-110);
    scene->addItem(block45);

    block46->setX(2750);
    block46->setY(-140);
    scene->addItem(block46);

    block47->setX(3100);
    block47->setY(-140);
    scene->addItem(block47);

    block48->setX(2766);
    block48->setY(-156);
    scene->addItem(block48);

    block49->setX(2782);
    block49->setY(-172);
    scene->addItem(block49);

    block50->setX(2798);
    block50->setY(-188);
    scene->addItem(block50);

    block51->setX(3100);
    block51->setY(-156);
    scene->addItem(block51);

    block52->setX(3100);
    block52->setY(-172);
    scene->addItem(block52);

    block53->setX(3100);
    block53->setY(-188);
    scene->addItem(block53);

    block54->setX(2862);
    block54->setY(-188);
    scene->addItem(block54);

    block55->setX(3200);
    block55->setY(-130);
    scene->addItem(block55);

    block56->setX(3300);
    block56->setY(-80);
    scene->addItem(block56);

    //Add blocks to the list
    listObjects_2->push_back(block1);
    listObjects_2->push_back(block2);
    listObjects_2->push_back(block3);
    listObjects_2->push_back(block4);
    listObjects_2->push_back(block5);
    listObjects_2->push_back(block6);
    listObjects_2->push_back(block7);
    listObjects_2->push_back(block8);
    listObjects_2->push_back(block9);
    listObjects_2->push_back(block10);
    listObjects_2->push_back(block11);
    listObjects_2->push_back(block12);
    listObjects_2->push_back(block13);
    listObjects_2->push_back(block14);
    listObjects_2->push_back(block15);
    listObjects_2->push_back(block16);
    listObjects_2->push_back(block17);
    listObjects_2->push_back(block18);
    listObjects_2->push_back(block19);
    listObjects_2->push_back(block20);
    listObjects_2->push_back(block21);
    listObjects_2->push_back(block22);
    listObjects_2->push_back(block23);
    listObjects_2->push_back(block24);
    listObjects_2->push_back(block25);
    listObjects_2->push_back(block26);
    listObjects_2->push_back(block27);
    listObjects_2->push_back(block28);
    listObjects_2->push_back(block29);
    listObjects_2->push_back(block30);
    listObjects_2->push_back(block31);
    listObjects_2->push_back(block32);
    listObjects_2->push_back(block33);
    listObjects_2->push_back(block34);
    listObjects_2->push_back(block35);
    listObjects_2->push_back(block36);
    listObjects_2->push_back(block37);
    listObjects_2->push_back(block38);
    listObjects_2->push_back(block39);
    listObjects_2->push_back(block40);
    listObjects_2->push_back(block41);
    listObjects_2->push_back(block42);
    listObjects_2->push_back(block43);
    listObjects_2->push_back(block44);
    listObjects_2->push_back(block45);
    listObjects_2->push_back(block46);
    listObjects_2->push_back(block47);
    listObjects_2->push_back(block48);
    listObjects_2->push_back(block49);
    listObjects_2->push_back(block50);
    listObjects_2->push_back(block51);
    listObjects_2->push_back(block52);
    listObjects_2->push_back(block53);
    listObjects_2->push_back(block54);
    listObjects_2->push_back(block55);
    listObjects_2->push_back(block56);
    listObjects_2->push_back(invisible_wall);
}

//Function to create the list of monsters for level 2
void Level::createListMonsters_2(QGraphicsScene *scene){
    this->listMonsters_2 = new QList<Monster*>();

    /*Monsters*/
    Monster *monster1 = new Monster(COVID);
    Monster *monster2 = new Monster(COVID);
    Monster *monster3 = new Monster(COVID);
    Monster *monster4 = new Monster(COVID);
    Monster *monster5 = new Monster(COVID);
    Monster *monster6 = new Monster(COVID);
    Monster *monster7 = new Monster(COVID);
    Monster *monster8 = new Monster(COVID);
    Monster *monster9 = new Monster(COVID);
    Monster *monster10 = new Monster(COVID);

    //Set the position
    monster1->setPos(400, -176);
    scene->addItem(monster1);

    monster2->setPos(620, -216);
    scene->addItem(monster2);

    monster3->setPos(700, -216);
    scene->addItem(monster3);

    monster4->setPos(400, -26);
    scene->addItem(monster4);

    monster5->setPos(1242, -78);
    scene->addItem(monster5);

    monster6->setPos(1402, -78);
    scene->addItem(monster6);

    monster7->setPos(1562, -78);
    scene->addItem(monster7);

    monster8->setPos(1722, -78);
    scene->addItem(monster8);

    monster9->setPos(1822, -78);
    scene->addItem(monster9);

    monster10->setPos(2042, -78);
    scene->addItem(monster10);

    //Add the monsters to the list
    listMonsters_2->push_back(monster1);
    listMonsters_2->push_back(monster2);
    listMonsters_2->push_back(monster3);
    listMonsters_2->push_back(monster4);
    listMonsters_2->push_back(monster5);
    listMonsters_2->push_back(monster6);
    listMonsters_2->push_back(monster7);
    listMonsters_2->push_back(monster8);
    listMonsters_2->push_back(monster9);
    listMonsters_2->push_back(monster10);
}

//Function to create the list of monsters for end levle
void Level::createListMonsters_end(QGraphicsScene *scene)
{
    /*Monsters*/
    //Pangolin
    Monster *pangolin_1 = new Monster(PANGOLIN);
    //Covid
    Monster *covid1 = new Monster(COVID);
    Monster *covid2 = new Monster(COVID);
    Monster *covid3 = new Monster(COVID);
    Monster *covid4 = new Monster(COVID);
    Monster *covid5 = new Monster(COVID);

    //Set the position of Covids
    covid1->setX(280);
    covid1->setY(-88);
    scene->addItem(covid1);

    covid2->setX(360);
    covid2->setY(-88);
    scene->addItem(covid2);

    covid3->setX(556);
    covid3->setY(-196);
    scene->addItem(covid3);

    covid4->setX(760);
    covid4->setY(-88);
    scene->addItem(covid4);

    covid5->setX(840);
    covid5->setY(-88);
    scene->addItem(covid5);

    //Set the position of pangolin
    pangolin_1->setX(600);
    pangolin_1->setY(-6);
    scene->addItem(pangolin_1);

    //Add the monsters to the list
    listMonsters_end = new QList<Monster*>();
    listMonsters_end->push_back(pangolin_1);
    listMonsters_end->push_back(covid1);
    listMonsters_end->push_back(covid2);
    listMonsters_end->push_back(covid3);
    listMonsters_end->push_back(covid4);
    listMonsters_end->push_back(covid5);
}

//Function to create the list of objects for end level
void Level::createListObjects_end(QGraphicsScene *scene)
{
    //Blocks
    QGraphicsPixmapItem *block1;
    QGraphicsPixmapItem *block2;
    QGraphicsPixmapItem *block3;
    QGraphicsPixmapItem *block5;
    QGraphicsPixmapItem *block6;
    QGraphicsPixmapItem *block7;
    QGraphicsPixmapItem *block9;
    QGraphicsPixmapItem *block10;
    QGraphicsPixmapItem *block11;
    QGraphicsPixmapItem *block12;
    QGraphicsPixmapItem *block13;
    QGraphicsPixmapItem *block15;
    QGraphicsPixmapItem *block16;

    //Set the position
    block1 = stair(1);
    block1->setX(125);
    block1->setY(-27);
    scene->addItem(block1);

    block2 = stair(13);
    block2->setX(200);
    block2->setY(-72);
    scene->addItem(block2);

    block3 = stair(1);
    block3->setX(216);
    block3->setY(-88);
    scene->addItem(block3);

    block5 = stair(1);
    block5->setX(376);
    block5->setY(-88);
    scene->addItem(block5);

    block6 = stair(8);
    block6->setX(500);
    block6->setY(-180);
    scene->addItem(block6);

    block7 = stair(1);
    block7->setX(516);
    block7->setY(-196);
    scene->addItem(block7);

    block9 = stair(1);
    block9->setX(596);
    block9->setY(-196);
    scene->addItem(block9);

    block10 = stair(1);
    block10->setX(430);
    block10->setY(-150);
    scene->addItem(block10);

    block11 = stair(1);
    block11->setX(682);
    block11->setY(-150);
    scene->addItem(block11);

    block12 = stair(13);
    block12->setX(720);
    block12->setY(-72);
    scene->addItem(block12);

    block13 = stair(1);
    block13->setX(736);
    block13->setY(-88);
    scene->addItem(block13);

    block15 = stair(1);
    block15->setX(896);
    block15->setY(-88);
    scene->addItem(block15);

    block16 = stair(1);
    block16->setX(987);
    block16->setY(-27);
    scene->addItem(block16);

    //Add the blocks to the list
    listObjects_end = new QList<QGraphicsPixmapItem*>();
    listObjects_end->push_back(block1);
    listObjects_end->push_back(block2);
    listObjects_end->push_back(block3);
    listObjects_end->push_back(block5);
    listObjects_end->push_back(block6);
    listObjects_end->push_back(block7);
    listObjects_end->push_back(block9);
    listObjects_end->push_back(block10);
    listObjects_end->push_back(block11);
    listObjects_end->push_back(block12);
    listObjects_end->push_back(block13);
    listObjects_end->push_back(block15);
    listObjects_end->push_back(block16);
}

//Function to create the list of boxes and bricks for end levle
void Level::createBoxesList_end(QGraphicsScene *scene)
{
    //Boxes
    Boxes *box0 = new Boxes(BOX);
    Boxes *box1 = new Boxes(BOX);
    Boxes *box2 = new Boxes(BOX);
    Boxes *box3 = new Boxes(BOX);
    Boxes *box4 = new Boxes(BOX);
    Boxes *box5 = new Boxes(BOX);
    Boxes *box6 = new Boxes(BOX);

    //Set the position
    box0->setX(248);
    box0->setY(-150);
    scene->addItem(box0);

    box1->setX(296);
    box1->setY(-150);
    scene->addItem(box1);

    box2->setX(344);
    box2->setY(-150);
    scene->addItem(box2);

    box3->setX(556);
    box3->setY(-255);
    scene->addItem(box3);

    box4->setX(768);
    box4->setY(-150);
    scene->addItem(box4);

    box5->setX(816);
    box5->setY(-150);
    scene->addItem(box5);

    box6->setX(864);
    box6->setY(-150);
    scene->addItem(box6);

    //Add the bricks and boxes to the list
    listBoxes_end = new QList<Boxes*>();
    listBoxes_end->push_back(box0);
    listBoxes_end->push_back(box1);
    listBoxes_end->push_back(box2);
    listBoxes_end->push_back(box3);
    listBoxes_end->push_back(box4);
    listBoxes_end->push_back(box5);
    listBoxes_end->push_back(box6);
}
