#ifndef MONSTER_H
#define MONSTER_H

#include <QGraphicsPixmapItem>
#include <QGraphicsScene>

//Types of monsters
enum monsterType {
    COVID,
    PANGOLIN
};

class Monster : public QGraphicsPixmapItem
{
public:
    Monster(monsterType type);

    //Check functions
    bool isLookingRight() {return right;}
    bool isLookingLeft() {return left;}
    void isDead(QList<Monster *> *listMonsters, QGraphicsScene *scene);

    //Update functions
    void hitMario();
    void moveMonster();

    //Getters
    int getLifeCounter() { return this->life_counter;}
    bool getHitMario() {return this->hit_mario;}
    bool getDead() {return this->dead;}
    monsterType getMonsterType() {return this->type;}
    QPixmap getTexture() {return this->texture;}
    QPixmap getTextureDead() {return this->texture_dead;}

    //Setters
    void setLeft(bool left) { this->left = left;}
    void setRight(bool right) { this->right = right;}
    void setWalkSpeed(int walk_speed) {this->walk_speed = walk_speed;}
    void setHitMario(bool hit_mario) {this->hit_mario = hit_mario;}
    void setDead();

private:
    //Textures for covids
    QPixmap texture;
    QPixmap texture_dead;

    //Textures for pangolin
    QPixmap pangolin;
    QPixmap pangolin_dead;

    //Attributes for monsters
    monsterType type;
    double walk_speed = 1;
    bool right;
    bool left;
    bool dead = false;
    int dead_time = 0;
    int life_counter = 1;
    bool hit_mario = false;
    int hit_time = 0;
    bool opacity = true;
};

#endif // MONSTER_H
