#include "masks.h"

Masks::Masks(QGraphicsItem *parent) : QGraphicsPixmapItem(parent)
{
    masks = 0;
    drawMask();
}

void Masks::gainMask()
{
    masks++;
}

void Masks::loseMask()
{
    masks--;
}

void Masks::drawMask()
{
    QString mask_path = ":/ui/ressources/mask_ui.png";
    QPixmap collage(mask_path);
    this->setPixmap(collage);
}
