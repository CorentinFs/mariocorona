#ifndef NIVEAU_H
#define NIVEAU_H

#include "boxes.h"
#include "consumable.h"
#include "monster.h"
#include "landscape.h"

#include <QGraphicsPixmapItem>
#include <QPainter>

class Level
{
private:
    //Attributes of all levels
    QList <Consumable*> *listConsumable;
    Landscape *landscape;
    QGraphicsPixmapItem *hospital = new QGraphicsPixmapItem();
    QGraphicsPixmapItem *clouds = new QGraphicsPixmapItem();

    //level 1
    QList <QGraphicsPixmapItem*> *listObjects_1;
    QList <Boxes*> *listBoxes_1;
    QList <Monster*> *listMonsters_1;
    QGraphicsPixmapItem *ground_1;
    QGraphicsPixmapItem *hillBush1 = new QGraphicsPixmapItem();

    //level2
    QList <QGraphicsPixmapItem*> *listObjects_2;
    QList <Boxes*> *listBoxes_2;
    QList <Monster*> *listMonsters_2;
    QGraphicsPixmapItem *ground_2_1;
    QGraphicsPixmapItem *ground_2_2;

    //end level
    QList <QGraphicsPixmapItem*> *listObjects_end;
    QList <Monster*> *listMonsters_end;
    QList <Boxes*> *listBoxes_end;
    QGraphicsPixmapItem *ground_end;
    QGraphicsPixmapItem *hillBush_end = new QGraphicsPixmapItem();

public:
    Level(QGraphicsScene *scene, int level);

    //Génération du terrain
    QGraphicsPixmapItem* groundMaker(int length, int width);
    QGraphicsPixmapItem* pipeSmall();
    QGraphicsPixmapItem* pipeMedium();
    QGraphicsPixmapItem* pipeBig();
    QGraphicsPixmapItem* hospitalEnd();
    QGraphicsPixmapItem* stair(int num_width);
    QGraphicsPixmapItem* invisibleWall();

    //Levels building
    //level 1
    void createBoxesList_1(QGraphicsScene *scene);
    void createListObjects_1(QGraphicsScene *scene);
    void createListMonsters_1(QGraphicsScene *scene);

    //level 2
    void createBoxesList_2(QGraphicsScene *scene);
    void createListObjects_2(QGraphicsScene *scene);
    void createListMonsters_2(QGraphicsScene *scene);

    //end level
    void createBoxesList_end(QGraphicsScene *scene);
    void createListObjects_end(QGraphicsScene *scene);
    void createListMonsters_end(QGraphicsScene *scene);

    //Getters per levels
    //All levels
    QList <Consumable*> *getListConsumable() {return listConsumable; }
    QGraphicsPixmapItem *getHospital() {return hospital; }
    QGraphicsPixmapItem *getClouds() {return clouds; }

    //level 1
    QList<QGraphicsPixmapItem*> *getListObjects_1() {return listObjects_1; }
    QList <Boxes*> *getListBoxes_1() {return listBoxes_1; }
    QList <Monster*> *getListMonsters_1() {return listMonsters_1; }
    QGraphicsPixmapItem *GetGround_1() {return ground_1; } 
    QGraphicsPixmapItem *getHillBush1() {return hillBush1; }

    //level 2
    QList<QGraphicsPixmapItem*> *getListObjects_2() {return listObjects_2; }
    QList <Boxes*> *getListBoxes_2() {return listBoxes_2; }
    QList <Monster*> *getListMonsters_2() {return listMonsters_2; }
    QGraphicsPixmapItem *GetGround_2_1() {return ground_2_1; }
    QGraphicsPixmapItem *GetGround_2_2() {return ground_2_2; }

    //end level
    QList <Monster*> *getListMonsters_end() {return listMonsters_end; }
    QList <Boxes*> *getListBoxes_end() {return listBoxes_end; }
    QList<QGraphicsPixmapItem*> *getListObjects_end() {return listObjects_end; }
    QGraphicsPixmapItem *GetGround_end() {return ground_end; }

};

#endif // NIVEAU_H
