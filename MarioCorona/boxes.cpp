#include "boxes.h"

/*Basic constructor for a box or a brick with no hit counter*/
Boxes::Boxes(typeBlock type)
{
    //Initilization of boxes type (brick or box)
    this->type = type;

    //Ressources image Brick with destruction
    if (type == BRICK){
        hitCounter = 0;
        QString brick1 = ":/levels/ressources/brick.bmp";
        brick_1 = QPixmap(brick1);
        QString brick2 = ":/levels/ressources/brick-debris.bmp";
        brick_2 = QPixmap(brick2);

        //Initialization of the pixmap
        setPixmap(brick_1);
    }
    //Ressources image Box with destrcution
    else {
        QString box0 = ":/levels/ressources/box-0.bmp";
        box_0 = QPixmap(box0);
        QString box1 = ":/levels/ressources/box-1.bmp";
        box_1 = QPixmap(box1);
        QString box2 = ":/levels/ressources/box-2.bmp";
        box_2 = QPixmap(box2);
        QString box3 = ":/levels/ressources/box-used.bmp";
        box_3 = QPixmap(box3);

        //Initialization of the pixmap
        setPixmap(box_0);
    }
}

/*Constructor for a box or a brick with hit counter*/
Boxes::Boxes(typeBlock type, int hitCounter)
{
    //Initilization of boxes type (brick or box)
    this->type = type;

    //Ressources image Brick avec destruction
    if (type == BRICK){
        //Initialization of the hit counter
        this->hitCounter = hitCounter;
        QString brick1 = ":/levels/ressources/brick.bmp";
        brick_1 = QPixmap(brick1);
        QString brick2 = R"(:/levels/ressources/brick-debris.bmp)";
        brick_2 = QPixmap(brick2);

        //Initialization of the pixmap
        setPixmap(brick_1);
    }
    //Ressources image Box avec destrcution
    else {
        QString box0 = ":/levels/ressources/box-0.bmp";
        box_0 = QPixmap(box0);
        QString box1 = ":/levels/ressources/box-1.bmp";
        box_1 = QPixmap(box1);
        QString box2 = ":/levels/ressources/box-2.bmp";
        box_2 = QPixmap(box2);
        QString box3 = ":/levels/ressources/box-used.bmp";
        box_3 = QPixmap(box3);

        //Initialization of the pixmap
        setPixmap(box_0);
    }
}

/*Method that allows the box to flash*/
void Boxes::animateBox(){
    //If the box is not destroyed and the type is BOX
    if (!destroyed && type == BOX){

        //For each state, we adjust the pixmap to make the box flash and we incremente the lightning state
        if(lightningState == 0){
            setPixmap(box_1);
            lightningState++;
        }
        else if (lightningState == 1){
            setPixmap(box_2);
            lightningState++;
        }
        else {
           setPixmap(box_0);
           lightningState = 0;
        }
    }
}

/*Method that destroy a box and make an animation of it*/
void Boxes::destroyBox() {
    //If the box is destroyed and the type is BOX
    if(destroyed && type == BOX){
        //For each state we we change the position of the box to make it animated and we incremente the animatedState
        if(animatedState == 0){
            setPixmap(box_3);
            setY(y()-4);
            animatedState++;
        }
        if ((animatedState < 7 && animatedState > 0) || (animatedState < 13 && animatedState > 7)){
            animatedState++;
        }
        if(animatedState == 7){
            setY(y()-3);
            animatedState++;
        }
        if(animatedState == 13){
            setY(y()+7);
            animatedState++;
        }
    }
}

/*Method that destroy a brick and make an animation of it*/
void Boxes::destroyBrick() {
    //If the box is destroyed and the type is BRICK
    if(destroyed && type == BRICK){
        //For each state we we change the position of the brick to make it animated and we incremente the animatedState
        if(animatedState == 0){
            setY(y()-4);
            animatedState++;
        }
        if ((animatedState < 7 && animatedState > 0) || (animatedState < 13 && animatedState > 7)){
            animatedState++;
        }
        if(animatedState == 7 && hitCounter == 0){
            setY(y()-3);
            setPixmap(brick_2);
            animatedState++;
        }
        if (animatedState == 7 && hitCounter != 0){
            setY(y()-3);
            animatedState++;
        }
        if(animatedState == 13 && hitCounter == 0){
            setY(y()+7);
            animatedState++;
        }
        //We reinitialize destroyed boolean to false if the hitcounter is not at 0
        if (animatedState == 13 && hitCounter != 0){
            setY(y()+7);
            animatedState = 0;
            destroyed = false;
            hitCounter--;
        }
    }
}

/*Getters and Setters*/
typeBlock Boxes::getTypeBlock(){
    return type;
}

void Boxes::setDestroyed(bool destroyed){
    this->destroyed = destroyed;
}

bool Boxes::isDestroyed(){
    return destroyed;
}

int Boxes::getAnimatedState(){
    return this->animatedState;
}

void Boxes::incrementeAnimatedState(){
    animatedState++;
}

