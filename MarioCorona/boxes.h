#ifndef BOXES_H
#define BOXES_H

#include <QGraphicsPixmapItem>

enum typeBlock {
    BOX,
    BRICK
};

class Boxes : public QGraphicsPixmapItem
{
public:
    //Constructors
    Boxes(typeBlock type);
    Boxes(typeBlock type, int hitCounter);

    //Animation and destrcution
    void animateBox();
    void destroyBox();
    void destroyBrick();

    //Getters and setters
    typeBlock getTypeBlock();
    void setDestroyed(bool destroyed);
    bool isDestroyed();
    int getAnimatedState();
    void incrementeAnimatedState();


private:
    //Texture
    QPixmap box_0;
    QPixmap box_1;
    QPixmap box_2;
    QPixmap box_3;
    QPixmap brick_1;
    QPixmap brick_2;

    //Attributes of the boxes
    int lightningState = 0;
    bool destroyed = false;
    int animatedState = 0;
    typeBlock type;
    int hitCounter = 0;

};

#endif // BOX_H

