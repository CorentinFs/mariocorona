#ifndef LANDSCAPE_H
#define LANDSCAPE_H

#include <QGraphicsPixmapItem>
#include <QPainter>
#include <QBitmap>
#include <QPixmap>
#include <QRandomGenerator>

class Landscape : public QGraphicsPixmapItem
{
public:
    Landscape();
    QGraphicsPixmapItem *createClouds();
    //Function to create landscape with bushes and hills for level1
    QGraphicsPixmapItem *createBushHill1();
    //Function to create landscape with bushes and hills for end level
    QGraphicsPixmapItem *createBushHillEnd();

private:
    int length = 3500;

    //Textures
    QPixmap single_cloud_small;
    QPixmap single_cloud_med;
    QPixmap single_cloud_big;
    QPixmap single_hill_small;
    QPixmap single_hill_big;
    QPixmap single_bush_small;
    QPixmap single_bush_med;
    QPixmap single_bush_big;

};

#endif // LANDSCAPE_H
