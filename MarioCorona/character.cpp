#include "character.h"

#include <QKeyEvent>

Character::Character()
{
    state = SMALL;

    //Ressources for Mario texture

    //Mario Normal
    QString s_mario_right = ":/characters/ressources/mario_normal.png";
    mario_right = QPixmap(s_mario_right);
    mario_left = mario_right.transformed(QTransform().scale(-1,1));

    QString s_mario_right_walk1 = ":/characters/ressources/mario_normal_walk1.png";
    mario_right_walk1 = QPixmap(s_mario_right_walk1);
    mario_left_walk1 = mario_right_walk1.transformed(QTransform().scale(-1,1));

    QString s_mario_right_walk2 = ":/characters/ressources/mario_normal_walk2.png";
    mario_right_walk2 = QPixmap(s_mario_right_walk2);
    mario_left_walk2 = mario_right_walk2.transformed(QTransform().scale(-1,1));

    QString s_mario_right_walk3 = ":/characters/ressources/mario_normal_walk3.png";
    mario_right_walk3 = QPixmap(s_mario_right_walk3);
    mario_left_walk3 = mario_right_walk3.transformed(QTransform().scale(-1,1));

    QString s_mario_jump_right = ":/characters/ressources/mario_normal_jump.png";
    mario_jump_right = QPixmap(s_mario_jump_right);
    mario_jump_left = mario_jump_right.transformed(QTransform().scale(-1,1));

    //Mario Small
    QString s_mario_small_right = ":/characters/ressources/mario_small.png";
    mario_small_right = QPixmap(s_mario_small_right);
    mario_small_left = mario_small_right.transformed(QTransform().scale(-1,1));

    QString s_mario_small_right_walk1 = ":/characters/ressources/mario_small_walk1.png";
    mario_small_right_walk1 = QPixmap(s_mario_small_right_walk1);
    mario_small_left_walk1 = mario_small_right_walk1.transformed(QTransform().scale(-1,1));

    QString s_mario_small_right_walk2 = ":/characters/ressources/mario_small_walk2.png";
    mario_small_right_walk2 = QPixmap(s_mario_small_right_walk2);
    mario_small_left_walk2 = mario_small_right_walk2.transformed(QTransform().scale(-1,1));

    QString s_mario_small_right_walk3 = ":/characters/ressources/mario_small_walk3.png";
    mario_small_right_walk3 = QPixmap(s_mario_small_right_walk3);
    mario_small_left_walk3 = mario_small_right_walk3.transformed(QTransform().scale(-1,1));

    QString s_mario_small_jump_right = ":/characters/ressources/mario_small_jump.png";
    mario_small_jump_right = QPixmap(s_mario_small_jump_right);
    mario_small_jump_left = mario_small_jump_right.transformed(QTransform().scale(-1,1));

    QString s_mario_dead = ":/characters/ressources/mario_dead.png";
    mario_dead = QPixmap(s_mario_dead);

    //Initialisation of the character
    if (state == SMALL) {
        setPixmap(mario_small_right);
    }
    if (state == NORMAL) {
        setPixmap(mario_right);
    }

    //Hit
    setHitMonster(false);
}

//Check if the character is looking to the left
bool Character::isLookingRight()
{
    if(pixmap().toImage() == mario_right.toImage()
            || pixmap().toImage() == mario_right_walk1.toImage()
            || pixmap().toImage() == mario_right_walk2.toImage()
            || pixmap().toImage() == mario_right_walk3.toImage()
            || pixmap().toImage() == mario_jump_right.toImage()
            || pixmap().toImage() == mario_small_right.toImage()
            || pixmap().toImage() == mario_small_jump_right.toImage()
            || pixmap().toImage() == mario_small_right_walk1.toImage()
            || pixmap().toImage() == mario_small_right_walk2.toImage()
            || pixmap().toImage() == mario_small_right_walk3.toImage()){
        return true;
    }
    return false;
}

//Check if the character is looking to the left
bool Character::isLookingLeft()
{
    if(pixmap().toImage() == mario_left.toImage()
            || pixmap().toImage() == mario_left_walk1.toImage()
            || pixmap().toImage() == mario_left_walk2.toImage()
            || pixmap().toImage() == mario_left_walk3.toImage()
            || pixmap().toImage() == mario_jump_left.toImage()
            || pixmap().toImage() == mario_small_left.toImage()
            || pixmap().toImage() == mario_small_jump_left.toImage()
            || pixmap().toImage() == mario_small_left_walk1.toImage()
            || pixmap().toImage() == mario_small_left_walk2.toImage()
            || pixmap().toImage() == mario_small_left_walk3.toImage()){
        return true;
    }
    return false;
}

//Le the character flash when hit by a monster
void Character::hitMonster() {
    if (hit_monster) {
         hit_time++;

         if (hit_time % 10 == 0){
             if(opacity){
                 setOpacity(0.3);
                 opacity = false;
             }
             else {
                 setOpacity(1);
                 opacity = true;
             }
         }
         //At the end of the flash, reset all the counter and states
         if (hit_time == 50) {
             hit_time = 0;
             setOpacity(1);
             opacity = true;
             hit_monster = false;
         }
    }
}

//Let the character do a little jump when he kill an ennemy
void Character::jumpMonster() {
    if (jump_monster) {
        if (hit_time == 0){
            jump_time++;
            setY(y()-1);
            jump = false;
            fall = false;
        }
        if ((jump_time > 0 && jump_time < 8) || (jump_time > 8 && jump_time < 16) || (jump_time > 16 && jump_time < 24)){
            jump_time++;
            jump = false;
            fall = false;
        }
        if(jump_time == 8){
            jump_time++;
            setY(y()-1);
            jump = false;
            fall = false;
        }
        if(jump_time == 16){
            jump_time++;
            setY(y()-1);
            jump = false;
            fall = false;
        }
        if(jump_time == 24){
            jump_time = 0;
            setY(y()-1);
            jump_monster = false;
            jump = false;
        }
    }
}

//Set the state of the character as invicible during a certain amount of time
void Character::invincible() {
    if (hit_monster) {
        hit_time++;
        //Make the character flash every 10 frames
        if (hit_time % 10 == 0){
            if(opacity){
                setOpacity(0.3);
                opacity = false;
            }
            else {
                setOpacity(1);
                opacity = true;
            }
        }
        //If the invincibility has ended, reset all the attributes
        if (hit_time == 800) {
            state = NORMAL;
            hit_time = 0;
            opacity = true;
            setOpacity(1);
            hit_monster = false;
        }
    }
}

//Update the position of the character depending on its state
void Character::moveCharacter()
{
    if(right)
    {
        if(jump){
            if(state == SMALL) {
                setPixmap(mario_small_jump_right);
            }
            if(state == NORMAL || state == INVINCIBLE) {
                setPixmap(mario_jump_right);
            }
            if(run)
                setX(pos().x() + walk_range_runing_jump_fall);
            else
                setX(pos().x() + walk_range_jumping);
        }
        else if(fall){
            if(state == SMALL) {
                setPixmap(mario_small_jump_right);
            }
            if(state == NORMAL || state == INVINCIBLE) {
                setPixmap(mario_jump_right);
            }
            if(run)
                setX(pos().x() + walk_range_runing_jump_fall);
            else
                setX(pos().x() + walk_range_jumping);
        }
        else if(run){
            setX(pos().x() + run_walk_range);
        }
        else{
            setX(pos().x() + walk_range);
        }
    }
    if(left)
    {
        if(jump){
            if(state == SMALL) {
                setPixmap(mario_small_jump_left);
            }
            if(state == NORMAL || state == INVINCIBLE) {
                setPixmap(mario_jump_left);
            }
            if(run)
                setX(pos().x() - walk_range_runing_jump_fall);
            else
                setX(pos().x() - walk_range_jumping);
        }
        else if(fall){
            if(state == SMALL) {
                setPixmap(mario_small_jump_left);
            }
            if(state == NORMAL || state == INVINCIBLE) {
                setPixmap(mario_jump_left);
            }
            if(run)
                setX(pos().x() - walk_range_runing_jump_fall);
            else
                setX(pos().x() - walk_range_jumping);
        }
        else if(run){
            setX(pos().x() - run_walk_range);
        }
        else{
            setX(pos().x() - walk_range);
        }
    }
    if(temp_jump_speed > 0 && jump){
        setY(pos().y() - temp_jump_speed);
        temp_jump_speed -= jump_descrease_speed;
    }
    else if(jump && temp_jump_speed <= 0)
    {
        jump = false;
        fall = true;
    }
    if(fall)
    {
        setY(pos().y() + temp_fall_speed);
        temp_fall_speed += fall_decrease_speed;
    }

    if(jump && !right && !left){
        if (isLookingRight()){
            if (state == SMALL){
                setPixmap(mario_small_jump_right);
            }
            if (state == NORMAL || state == INVINCIBLE){
                setPixmap(mario_jump_right);
            }
        }
        if (isLookingLeft()){
            if (state == SMALL){
                setPixmap(mario_small_jump_left);
            }
            if (state == NORMAL || state == INVINCIBLE){
                setPixmap(mario_jump_left);
            }
        }
    }
}

//Update the texture of the character depending on the new state of it.
void Character::changeTexture()
{
   if(state == SMALL){
       //Set to right depending on the current state
       if(right && !jump && !fall){
           if (walking_state == 0){
               setPixmap(mario_small_right_walk1);
               walking_state = 1;
           }
           else if (walking_state == 1){
               setPixmap(mario_small_right_walk2);
               walking_state = 2;
           }
            else {
               setPixmap(mario_small_right_walk3);
               walking_state = 0;
           }
       }
       //Set the left depending on the current state
       if(left && !jump && !fall){
           if (walking_state == 0){
               setPixmap(mario_small_left_walk1);
               walking_state = 1;
           }
           else if (walking_state == 1){
               setPixmap(mario_small_left_walk2);
               walking_state = 2;
           }
           else {
               setPixmap(mario_small_left_walk3);
               walking_state = 0;
           }
       }
       //Set to static depending on the current state
       if (!right && !left && !jump && !fall){
           walking_state = 0;
           if(isLookingLeft()){
               setPixmap(mario_small_left);
           }
           if(isLookingRight()){
               setPixmap(mario_small_right);
           }
       }
   }
   if(state == NORMAL || state == INVINCIBLE){
       //Set to right depending on the current state
       if(right && !jump && !fall){
           if (walking_state == 0){
               setPixmap(mario_right_walk1);
               walking_state = 1;
           }
           else if (walking_state == 1){
               setPixmap(mario_right_walk2);
               walking_state = 2;
           }
           else {
               setPixmap(mario_right_walk3);
               walking_state = 0;
           }
       }
       //Set to left depending on the current state
       if(left && !jump && !fall){
           if (walking_state == 0){
               setPixmap(mario_left_walk1);
               walking_state = 1;
           }
           else if (walking_state == 1){
               setPixmap(mario_left_walk2);
               walking_state = 2;
           }
           else {
               setPixmap(mario_left_walk3);
               walking_state = 0;
           }
       }
       //Set to static depending on the current state
       if (!right && !left && !jump && !fall){
           walking_state = 0;
           if(isLookingLeft()){
               setPixmap(mario_left);
           }
           if(isLookingRight()){
               setPixmap(mario_right);
           }
       }
   }
}
