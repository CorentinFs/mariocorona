#include "monster.h"

Monster::Monster(monsterType type)
{
    //Set the type of the monster
    this->type = type;

    //Case of covids
    if (type == COVID){
        //Life counter set to 0 for covids
        life_counter = 0;

        //Textures
        QString covid = ":/levels/ressources/covid.png";
        this->texture = QPixmap(covid);
        QString covid_dead = ":/levels/ressources/covid-dead.png";
        this->texture_dead = QPixmap(covid_dead);
        setPixmap(covid);

        //Initialisation of left direction for covids
        this->left = true;
        this->right = false;
    }

    //Case of pangolin
    else {
        //Life counter set to 5 for pangolin and and walk_speed update to 1.5 insteed of 1
        walk_speed = 1.5;
        life_counter = 5;

        //Textures
        QString pangolin_image = ":/characters/ressources/pangolin.png";
        this->pangolin = QPixmap(pangolin_image);
        QString pangolin_dead_image = ":/characters/ressources/pangolin_dead.png";
        this->pangolin_dead = QPixmap(pangolin_dead_image);
        setPixmap(pangolin);

        //Initialisation of left direction for pangolin
        this->left = true;
        this->right = false;
    }
}

//Function to set a monster to dead
void Monster::setDead() {
    if (life_counter == 0) {
        //Stop the movements of the monster
        this->setLeft(false);
        this->setRight(false);

        //Update of texture for covid
        if (type == COVID){
            this->setPixmap(this->getTextureDead());
            this->setPos(this->x(), this->y() + (this->pixmap().width()/2));
        }

        //Update of texture for pangolin
        if (type == PANGOLIN){
            this->setPixmap(this->pangolin_dead);
            this->setPos(this->x(), 20);
        }
        //Update of the dead boolean to true
        this->dead = true;
    }
    //For pangolin
    else {
        this->hit_mario = true;
        this->dead = true;
    }
}

//Function to call when a monster hit mario
void Monster::hitMario() {
    if (this->hit_mario) {
        this->hit_time++;

        if (hit_time % 10 == 0){
            if(opacity){
                setOpacity(0.3);
                opacity = false;
            }
            else {
                setOpacity(1);
                opacity = true;
            }
        }

        if (this->hit_time == 50) {
            this->walk_speed = 1.5;
            this->hit_time = 0;
            this->hit_mario = false;
            setOpacity(1);
            opacity = true;
        }
    }
}

//Function to check if a monster is dead and delete the monster if he is dead
void Monster::isDead(QList<Monster*> *listMonsters, QGraphicsScene *scene) {
    if(life_counter == 0){
        //Updating dead_time to let the dead monster a few moment on the screen
        if (this->dead) {
            this->dead_time++;
        }
        //Removing the monster when dead_time equals 40
        if (this->dead_time == 40) {
            if(type == COVID){
                listMonsters->removeOne(this);
                scene->removeItem(this);
            }
        }
    }
    //Pangolin
    else {
        //Updating lifes of the pangolin
        if (this->dead){
            this->dead = false;
            this->life_counter--;
        }
    }
}

//Function to make a monster moving
void Monster::moveMonster()
{
    //Covids
    if(type == COVID){
        if(right)
        {
            //Update of the position
            setX(this->pos().x() + walk_speed);
        }
        if(left)
        {
            //Update of the position
            setX(this->pos().x() - walk_speed);
        }
    }
    //Pangolin
    else {
        if(right)
        {
            //Update of the position
            setX(this->pos().x() + walk_speed);
        }
        if(left)
        {
            //Update of the position
            setX(this->pos().x() - walk_speed);
        }
    }
}

