#ifndef MASKS_H
#define MASKS_H

#include <QGraphicsPixmapItem>
#include <QFont>

class Masks : public QGraphicsPixmapItem
{
public:
    Masks(QGraphicsItem * parent=0);

    //Getter and setter on the masks attributes
    void gainMask();
    void loseMask();
    void drawMask();
    int getMasks() { return masks; }
    void setMasks(int masks) {this->masks = masks;}
private:
    int masks;
};

#endif // MASKS_H
