QT       += core gui
QT       += multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    boxes.cpp \
    character.cpp \
    collisiondirection.cpp \
    consumable.cpp \
    game.cpp \
    landscape.cpp \
    level.cpp \
    life.cpp \
    main.cpp \
    masks.cpp \
    monster.cpp

HEADERS += \
    boxes.h \
    character.h \
    collisiondirection.h \
    consumable.h \
    game.h \
    landscape.h \
    level.h \
    masks.h \
    monster.h \
    life.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    ressources/Trump.png \
    ressources/basic_music.wav \
    ressources/block-hit.wav \
    ressources/block.png \
    ressources/box-0.bmp \
    ressources/box-1.bmp \
    ressources/box-2.bmp \
    ressources/box-used.bmp \
    ressources/brick-debris.bmp \
    ressources/brick.bmp \
    ressources/bush-big.png \
    ressources/bush-med.png \
    ressources/bush-small.png \
    ressources/cloud-big.png \
    ressources/cloud-med.png \
    ressources/cloud-small.png \
    ressources/consumable.wav \
    ressources/consumable_eat.wav \
    ressources/covid-dead.png \
    ressources/covid.png \
    ressources/gameover.wav \
    ressources/hill-big.png \
    ressources/hill-small.png \
    ressources/hopital.png \
    ressources/jump_normal.wav \
    ressources/jump_small.wav \
    ressources/mario-big-stand.png \
    ressources/mario_normal.png \
    ressources/mario_normal_jump.png \
    ressources/mario_normal_walk1.png \
    ressources/mario_normal_walk2.png \
    ressources/mario_normal_walk3.png \
    ressources/mario_small.png \
    ressources/mario_small_dead.png \
    ressources/mario_small_jump.png \
    ressources/mario_small_walk1.png \
    ressources/mario_small_walk2.png \
    ressources/mario_small_walk3.png \
    ressources/mask.png \
    ressources/mask.wav \
    ressources/mask_ui.png \
    ressources/monster.wav \
    ressources/pangolin_heart.png \
    ressources/pause.wav \
    ressources/pipe-big.png \
    ressources/pipe-med.png \
    ressources/pipe-small.png \
    ressources/seringue_Covid.png \
    ressources/small.wav \
    ressources/star.png \
    ressources/wall.png

RESOURCES += \
    marioCorona.qrc
