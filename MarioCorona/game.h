#ifndef GAME_H
#define GAME_H

#include "character.h"
#include "level.h"
#include "collisiondirection.h"
#include "landscape.h"
#include "monster.h"
#include "life.h"
#include "masks.h"
#include "boxes.h"
#include "consumable.h"

#include <time.h>
#include <QGraphicsView>
#include <QTimer>
#include <QDesktopWidget>
#include <QGraphicsScene>
#include <QKeyEvent>
#include <QGraphicsItem>
#include <QSound>
#include <QApplication>

#include <QDebug>

enum gameState{
    ENDING_LEVEL,
    PLAYING,
    PAUSE
};

class Game : public QGraphicsView
{
    Q_OBJECT

public:
    Game();

    void checkMarioFalling();
    void checkMarioCollidingBlock();
    void checkMarioCollidingConsumable();
    void checkMonsterColliding(Monster* monster);
    void checkMarioCollidingMonster();
    void checkMarioEndLevel();
    void checkMarioFallUnderGround();
    bool checkMarioHitingPangolin(Monster *monster);
    void endingLevel();
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void monsterDead(Monster* monster);
    void setUpUI();
    void updateUI();
    void gameover();
    void togglePause();
    void spawn();
    void closeGame();

public slots:
    void move();
    void lightBoxes();
    void textureWalking();

private:
    //Sound
    QSound *sound;

    //Mario spawn position
    int mario_spawn_x_1 = 16;
    int mario_spawn_x_2 = 300;
    int mario_spawn_y = 0;
    int current_level;

    //Game state
    gameState state = PLAYING;
    bool marioHasRespawn;
    QGraphicsScene *scene;
    QTimer engine;
    QTimer engineBoxes1;
    QTimer engineTextureWalking;
    Character *mario;
    Level *niveau;
    CollisionDirection collisionDir;

    //Lists of the objects in the current level
    QList <QGraphicsPixmapItem*> *listObjects = new QList <QGraphicsPixmapItem*>();
    QList <Boxes*> *listBoxes = new QList <Boxes*>();
    QList <Monster*> *listMonsters = new QList <Monster*>();
    QList <Consumable*> *listConsumable = new QList <Consumable*>();
    QList<QGraphicsPixmapItem*> *ground = new QList<QGraphicsPixmapItem*>();

    //All displays
    QGraphicsTextItem *life_text;
    QGraphicsTextItem *pieces_text;
    QGraphicsTextItem *success_text;
    QGraphicsTextItem *pause_text;
    QGraphicsTextItem *boss_life_text = new QGraphicsTextItem();
    Life *life;
    Masks *pieces;
    Life *boss_life = new Life();
    QGraphicsPixmapItem *trump_Message;

    //Counter for the between-level screen (in ms)
    int total_switch_time = 4*60*16;
    int temp_switch_time = 0;

    //Coordinates of end levels
    int end_x_1 = 3336;
    int end_y_1 = -150;
    int end_x_2 = 3400;
    int end_y_2 = -36;
};

#endif // GAME_H
