#ifndef CONSUMABLE_H
#define CONSUMABLE_H

#include "boxes.h"

#include <QGraphicsPixmapItem>

enum typeConsumable {
    SYRINGE,
    MASK,
    STAR
};

class Consumable: public QGraphicsPixmapItem
{
public:
    Consumable(typeConsumable type);

    //Animation
    void animateMask();

    //Getters and setters
    typeConsumable getTypeConsumable();
    int getAnimatedState();

private:
    //Textures
    QPixmap syringe;
    QPixmap mask;
    QPixmap star;

    //Attributes of the consumable
    typeConsumable type;
    int animatedState = 0;
};

#endif // CONSUMABLE_H
