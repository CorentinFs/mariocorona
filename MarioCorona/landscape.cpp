#include "landscape.h"

Landscape::Landscape()
{
    //Initialisation of textures
    QString cloud_small = ":/levels/ressources/cloud-small.png";
    single_cloud_small = QPixmap(cloud_small);

    QString cloud_med = ":/levels/ressources/cloud-med.png";
    single_cloud_med = QPixmap(cloud_med);

    QString cloud_big = ":/levels/ressources/cloud-big.png";
    single_cloud_big = QPixmap(cloud_big);

    QString bush_small = ":/levels/ressources/bush-small.png";
    single_bush_small = QPixmap(bush_small);

    QString bush_med = ":/levels/ressources/bush-med.png";
    single_bush_med = QPixmap(bush_med);

    QString bush_big = ":/levels/ressources/bush-big.png";
    single_bush_big = QPixmap(bush_big);

    QString hill_small = ":/levels/ressources/hill-small.png";
    single_hill_small = QPixmap(hill_small);

    QString hill_big = ":/levels/ressources/hill-big.png";
    single_hill_big = QPixmap(hill_big);
}

//Function to create clouds for each level
QGraphicsPixmapItem *Landscape::createClouds() {
    QGraphicsPixmapItem *result = new QGraphicsPixmapItem();

    //Pixmap for clouds
    QPixmap collage(3500, 50);
    collage.fill(Qt::transparent);
    //Painter to contain all the clouds
    QPainter painter(&collage);

    //Adding small clouds to the painter
    painter.drawPixmap(80, 0, single_cloud_big);
    for(int x=0; x<length; x += single_cloud_small.width() + 400)
        painter.drawPixmap(x, 0, single_cloud_small);

    //Adding medium clouds to the painter
    for(int x=300; x<length; x += single_cloud_med.width() + 400)
        painter.drawPixmap(x, 10, single_cloud_med);

    //Adding big clouds to the painter
    for(int x=500; x<length; x += single_cloud_big.width() + 400)
        painter.drawPixmap(x, 20, single_cloud_big);

    //Setting the pixmap result with collage
    result->setPixmap(collage);
    return result;
}

//Function to create bushes and hills for level 1
QGraphicsPixmapItem *Landscape::createBushHill1() {
    QGraphicsPixmapItem *result = new QGraphicsPixmapItem();

    //Pixmap for bushes and hills
    QPixmap collage(3500, 50);
    collage.fill(Qt::transparent);
    //Painter to contain all the bushes and hills
    QPainter painter(&collage);

    //Adding bushes and hills to the painter
    painter.drawPixmap(80, 0, single_hill_big);
    painter.drawPixmap(120, 13, single_hill_small);
    painter.drawPixmap(250, 16, single_bush_med);
    painter.drawPixmap(400, 16, single_bush_big);
    painter.drawPixmap(523, 13, single_hill_small);
    painter.drawPixmap(730, 0, single_hill_big);
    painter.drawPixmap(805, 16, single_bush_small);
    painter.drawPixmap(1000, 16, single_bush_med);
    painter.drawPixmap(1048, 13, single_hill_small);
    painter.drawPixmap(1300, 0, single_hill_big);
    painter.drawPixmap(1430, 16, single_bush_big);
    painter.drawPixmap(1494, 13, single_hill_small);
    painter.drawPixmap(1600, 16, single_bush_med);
    painter.drawPixmap(1700, 16, single_bush_big);
    painter.drawPixmap(1950, 0, single_hill_big);
    painter.drawPixmap(2190, 16, single_bush_small);
    painter.drawPixmap(2260, 16, single_bush_small);
    painter.drawPixmap(2330, 16, single_bush_small);
    painter.drawPixmap(2800, 0, single_hill_big);
    painter.drawPixmap(2900, 16, single_bush_small);
    painter.drawPixmap(2950, 0, single_hill_big);
    painter.drawPixmap(3050, 16, single_bush_small);
    painter.drawPixmap(3100, 0, single_hill_big);
    painter.drawPixmap(2720, 16, single_bush_big);
    painter.drawPixmap(3200, 16, single_bush_big);

    //Setting the pixmap result with collage
    result->setPixmap(collage);
    return result;
}

//Function to create bushes and hills for end level
QGraphicsPixmapItem *Landscape::createBushHillEnd() {
    QGraphicsPixmapItem *result = new QGraphicsPixmapItem();

    //Pixmap for bushes and hills
    QPixmap collage(3500, 50);
    collage.fill(Qt::transparent);
    //Painter to contain all the bushes and hills
    QPainter painter(&collage);

    //Adding bushes and hills to the painter
    painter.drawPixmap(300, 16, single_bush_small);
    painter.drawPixmap(450, 13, single_hill_small);
    painter.drawPixmap(498, 16, single_bush_med);
    painter.drawPixmap(620, 0, single_hill_big);
    painter.drawPixmap(660, 13, single_hill_small);
    painter.drawPixmap(750, 16, single_bush_big);

    //Setting the pixmap result with collage
    result->setPixmap(collage);
    return result;
}


