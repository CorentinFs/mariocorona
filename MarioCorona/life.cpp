#include "life.h"

Life::Life(QGraphicsItem * parent) :QGraphicsPixmapItem(parent)
{
    life = 4;
    drawLifes();
}

void Life::loseLife()
{
    if(life > 0){
        life--;
        drawLifes();
    }
}

void Life::gainLife()
{
    life++;
    drawLifes();
}

void Life::setPangolinHeart()
{
    //Get the pixmap and add it to the current PixmapItem
    QString pangolin_heart_path = ":/ui/ressources/pangolin_heart.png";
    QPixmap collage(pangolin_heart_path);
    setPixmap(collage);
}

void Life::drawLifes()
{
    //Get the pixmap and add it to the current PixmapItem
    QString heart_path = ":/ui/ressources/heart.png";
    QPixmap collage(heart_path);
    setPixmap(collage);
}
