#include "game.h"

#include <QDateTime>

Game::Game()
{
    //Create the scene
    scene = new QGraphicsScene();
    scene->setBackgroundBrush(QBrush(QColor(99,133,251)));
    scale(2.0,2.0);
    scene->setSceneRect(0, -238, 3500, 225);

    // setup game engine
    QObject::connect(&engine, SIGNAL(timeout()), this, SLOT(move()));
    engine.setTimerType(Qt::PreciseTimer);
    engine.setInterval(16);

    // setup lightning boxes engine
    QObject::connect(&engineBoxes1, SIGNAL(timeout()), this, SLOT(lightBoxes()));
    engineBoxes1.setTimerType(Qt::PreciseTimer);
    engineBoxes1.setInterval(220);

    // setup lightning boxes engine
    QObject::connect(&engineTextureWalking, SIGNAL(timeout()), this, SLOT(textureWalking()));
    engineTextureWalking.setTimerType(Qt::PreciseTimer);
    engineTextureWalking.setInterval(130);

    //Set up the view
    QDesktopWidget *desktopWidget = new QDesktopWidget();
    QRect screenSize = desktopWidget->availableGeometry(this);
    setHorizontalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );
    setVerticalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );
    setFixedSize(QSize(screenSize.width() * 1.f, screenSize.height() * 1.f));

    //Initialise Mario
    mario = new Character();
    mario->setPos(mario_spawn_x_1, mario_spawn_y);
    scene->addItem(mario);
    marioHasRespawn = false;

    //Spawn the first level of the game
    current_level = 1;
    spawn();

    //Add Life and Pieces Text
    life_text = new QGraphicsTextItem();
    pieces_text = new QGraphicsTextItem();

    life = new Life();
    pieces = new Masks();

    //Set up the random for star spawn
    srand(time(NULL));

    //Set up the UI for life and masks
    setUpUI();

    scene->addItem(life_text);
    scene->addItem(life);
    scene->addItem(pieces);
    scene->addItem(pieces_text);

    setScene(scene);

    //Game music
    sound = new QSound(":/sounds/ressources/basic_music.wav");
    sound->play();
    sound->setLoops(QSound::Infinite);

    engine.start();
    engineBoxes1.start();
    engineTextureWalking.start();
}

/* Method to animate the scene */
void Game::move()
{
    //Check if mario has ended the level
    if (state != ENDING_LEVEL){
        checkMarioEndLevel();
    }

    //Stop the game if mario is dead
    if(mario->getState() == DEAD){
        gameover();
    }
    //Finish the level if mario has reach the end
    else if(state == ENDING_LEVEL){
        endingLevel();
    }
    else{
        //Check for each collision
        checkMarioFalling();
        checkMarioCollidingMonster();
        checkMarioCollidingBlock();
        checkMarioCollidingConsumable();

        //Check if mario is jumping on a monster
        mario->jumpMonster();

        //For each monster, check if he's dead
        for (int monster = 0; monster < listMonsters->size(); monster++) {
            listMonsters->at(monster)->isDead(listMonsters, scene);
        }

        //For eache boxes, check if it's destroyed
        for(Boxes *box : *listBoxes){
            box->destroyBox();
            box->destroyBrick();
            if(box->isDestroyed() && box->getTypeBlock() == BRICK && box->getAnimatedState() == 14){
                scene->removeItem(box);
                listBoxes->removeOne(box);
            }
            if(box->isDestroyed() && box->getTypeBlock() == BOX && box->getAnimatedState() == 14){
                if ((rand() % 7) + 1 == 7) {
                    Consumable *consumable = new Consumable(STAR);
                    consumable->setX(box->x()+1);
                    consumable->setY(box->y() - box->pixmap().height()+2);
                    scene->addItem(consumable);
                    listConsumable->push_back(consumable);
                    box->incrementeAnimatedState();
                }
                else {
                    Consumable *consumable = new Consumable(SYRINGE);
                    consumable->setX(box->x()+1);
                    consumable->setY(box->y() - box->pixmap().height()+2);
                    scene->addItem(consumable);
                    listConsumable->push_back(consumable);
                    box->incrementeAnimatedState();
                }
            }
        }

        //For each consumable check if it's touch
        for (Consumable *consumable : *listConsumable) {
            consumable->animateMask();
            if(consumable->getAnimatedState() == 20){
                listConsumable->removeOne(consumable);
                scene->removeItem(consumable);
                pieces->gainMask();

                if(pieces->getMasks() == 30){
                    life->gainLife();
                    mario->gainLife();

                    //Reset the number of pieces
                    pieces->setMasks(0);
                }
            }
        }

        //Check if mario is moving
        mario->moveCharacter();

        //For each monster, make him moving, check for collision and special case for the pangolin
        for (int monster = 0; monster < listMonsters->size(); monster++) {

            if (!listMonsters->at(monster)->getDead()) {
                checkMonsterColliding(listMonsters->at(monster));
                listMonsters->at(monster)->moveMonster();
            }

            //Make the pangolin back if he reaches the end of the ground
            if (listMonsters->at(monster)->getMonsterType() == PANGOLIN){
                if (listMonsters->at(monster)->x() < 200){
                    listMonsters->at(monster)->setPixmap(listMonsters->at(monster)->pixmap().transformed(QTransform().scale(-1,1)));
                    listMonsters->at(monster)->setLeft(false);
                    listMonsters->at(monster)->setRight(true);
                }
                if (listMonsters->at(monster)->x() > 860){
                    listMonsters->at(monster)->setPixmap(listMonsters->at(monster)->pixmap().transformed(QTransform().scale(-1,1)));
                    listMonsters->at(monster)->setLeft(true);
                    listMonsters->at(monster)->setRight(false);
                }
            }

            //Make the pangolin invincible for a little time if he was hit by mario
            if (listMonsters->at(monster)->getMonsterType() == PANGOLIN && listMonsters->at(monster)->getHitMario()){
                mario->hitMonster();
                listMonsters->at(monster)->hitMario();
            }
        }

        //Check if mario is not falling under the ground
        checkMarioFallUnderGround();

        //Keep the view center on mario
        centerOn(mario);

        //Update the UI at each frame
        updateUI();
    }
}

/* Mehtod to change the mario's texture if he's walking (connected to a timer)*/
void Game::textureWalking(){
    mario->changeTexture();
}

/* Method to stop the game if the player lose all lifes */
void Game::gameover()
{
    //Sounds
    this->sound->stop();
    QSound::play(":/sounds/ressources/gameover.wav");

    //Stop all the engines
    engine.stop();
    engineBoxes1.stop();

    //Set the background to black
    scene->setBackgroundBrush(QBrush(Qt::black));

    //Display a message in the middle of the screen
    QGraphicsTextItem *gameoverText = new QGraphicsTextItem();
    gameoverText->setPlainText("GAME OVER");
    gameoverText->setDefaultTextColor(Qt::white);
    gameoverText->setFont(QFont("times", 56));

    //Compute the offset of the text to be placed in the center of the screen
    QPointF viewCenterPoint = mapToScene(QPoint(this->rect().x(), this->rect().y()));
    int pos_x = viewCenterPoint.x() + this->rect().width()/4 -  gameoverText->boundingRect().width() / 2;
    gameoverText->setX(pos_x);
    gameoverText->setY(-200);
    scene->addItem(gameoverText);
}

/* Method to pause the game if the player press escape */
void Game::togglePause()
{
    //Set the state of the game when the player has pressed Escape button
    if(state == PLAYING){
        //Sounds
        this->sound->stop();
        QSound::play(":/sounds/ressources/pause.wav");
        state = PAUSE;

        //Display the PAUSE screen and pause the engines
        engine.stop();
        engineBoxes1.stop();

        pause_text = new QGraphicsTextItem();
        pause_text->setPlainText("PAUSE");
        pause_text->setDefaultTextColor(Qt::white);
        pause_text->setFont(QFont("times", 56));

        QPointF viewCenterPoint = mapToScene(QPoint(this->rect().x(), this->rect().y()));
        int pos_x = viewCenterPoint.x() + this->rect().width()/4 -  pause_text->boundingRect().width() / 2;
        pause_text->setX(pos_x);
        pause_text->setY(-200);

        scene->addItem(pause_text);
    }
    else if(state == PAUSE){
        //Sound
        this->sound->play();

        state = PLAYING;

        //Restart the engine and delete the pause screen
        engine.start();
        engineBoxes1.start();

        scene->removeItem(pause_text);
    }
}

/* Method to update the UI*/
void Game::updateUI()
{
    QPointF viewCenterPoint = mapToScene(QPoint(this->rect().x(), this->rect().y()));

    life_text->setPlainText(QString("x") + QString::number(life->getLifes()));
    pieces_text->setPlainText(QString("x") + QString::number(pieces->getMasks()));

    pieces_text->setX(viewCenterPoint.x() + 35);
    life_text->setX(viewCenterPoint.x() + 35);
    life->setX(viewCenterPoint.x() + 1);
    pieces->setX(viewCenterPoint.x() + 1);

    if(current_level == 3){
        //Get the number of life of the Boss
        int number_lifes = 0;
        for(int i = 0; i < listMonsters->size(); i++){
            if(listMonsters->at(i)->getMonsterType() == PANGOLIN){
                number_lifes = listMonsters->at(i)->getLifeCounter();
                break;
            }
        }

        //Place the boss UI and set up the text
        boss_life_text->setPlainText(QString("x") + QString::number(number_lifes));

        QPointF viewCenterPoint = mapToScene(QPoint(this->rect().x(), this->rect().y()));
        boss_life_text->setX(viewCenterPoint.x() + this->rect().width() / 2
                             - boss_life->boundingRect().width() - boss_life_text->boundingRect().width() - 3);

        boss_life->setX(viewCenterPoint.x() + this->rect().width() / 2
                   - boss_life->boundingRect().width() - 3);
    }
}

/* Mehtod to initialize the UI */
void Game::setUpUI()
{
    //Set up the TextItems
    life_text->setPlainText(QString("x") + QString::number(life->getLifes()));
    life_text->setDefaultTextColor(Qt::white);
    life_text->setFont(QFont("times", 16));

    pieces_text->setPlainText(QString("x") + QString::number(pieces->getMasks()));
    pieces_text->setDefaultTextColor(Qt::white);
    pieces_text->setFont(QFont("times", 16));

    //Center the text in the view ?
    QPointF viewCenterPoint = mapToScene(QPoint(this->rect().x(), this->rect().y()));
    life_text->setX(viewCenterPoint.x() + 35);
    life_text->setY(-250);

    life->setX(viewCenterPoint.x());
    life->setY(-250);

    pieces->setX(viewCenterPoint.x());
    pieces->setY(-225);

    pieces_text->setX(viewCenterPoint.x() + 35);
    pieces_text->setY(-225);

    //Add the boss life if we are in the last level
    boss_life_text->setPlainText(QString("x"));
    boss_life_text->setDefaultTextColor(Qt::white);
    boss_life_text->setFont(QFont("times", 16));

    //Place the boss UI
    boss_life_text->setX(viewCenterPoint.x() + this->rect().width() / 2
                         - boss_life->boundingRect().width() - boss_life_text->boundingRect().width());
    boss_life_text->setY(-250);

    boss_life->setPangolinHeart();
    boss_life->setX(viewCenterPoint.x() + this->rect().width() / 2
               - boss_life->boundingRect().width());
    boss_life->setY(-250);
}

/* Method to make each box flash (connected to a timer) */
void Game::lightBoxes()
{
    for (int box = 0; box < listBoxes->size(); box++) {
        listBoxes->at(box)->animateBox();
    }
}

/*Collision management with box or brick*/
void Game::checkMarioCollidingBlock() {
    //For each boxes of the scene
    for ( int box = 0; box <listBoxes->size(); box++ ){
        if(mario->collidesWithItem(listBoxes->at(box)))
        {
            //Get the collision's direction on x and y
            collision_dir dir = collisionDir.GetCollisionDirX(mario, listBoxes->at(box));
            bool down = collisionDir.GetCollisionDown(mario, listBoxes->at(box), mario->getFallSpeed());
            bool up = collisionDir.GetCollisionUp(mario, listBoxes->at(box), mario->getJumpSpeed());

            //Make direction false if the collision is on the x axis
            if(dir == LEFT && !down && !up) {
                mario->setLeft(false);
            }
            if(dir == RIGHT && !down && !up) {
                mario->setRight(false);
            }

            //Make fall to false if the collion is above the block
            if(down){
                mario->setFall(false);
                //Realign mario with the block
                mario->setY(listBoxes->at(box)->y() - mario->boundingRect().height() + 1);
            }

            //Make mario falling if he hit the block behind
            if(up){
                mario->setJumping(false);
                mario->setFall(true);
                //If the block is a brick, make appear a Mask and animate it
                if (listBoxes->at(box)->getTypeBlock() == BRICK) {
                    QSound::play(":/sounds/ressources/block-hit.wav");
                    Consumable *consumable = new Consumable(MASK);
                    consumable->setX(listBoxes->at(box)->x());
                    consumable->setY(listBoxes->at(box)->y() - listBoxes->at(box)->pixmap().height());
                    scene->addItem(consumable);
                    listConsumable->push_back(consumable);
                    QSound::play(":/sounds/ressources/mask.wav");
                }
                //Else, it's a box so we destroy it
                else {
                    QSound::play(":/sounds/ressources/block-hit.wav");
                    if (!listBoxes->at(box)->isDestroyed()) {
                        QSound::play(":/sounds/ressources/consumable.wav");
                    }
                }
                listBoxes->at(box)->setDestroyed(true);
            }
        }
    }
}

/*Collision management with consumable*/
void Game::checkMarioCollidingConsumable() {
    //For each consumable of the scene
    for ( Consumable *consumable : *listConsumable){
        if(mario->collidesWithItem(consumable))
        {
            //We get the position of Mario because we will use it later
            int y = mario->y() + mario->pixmap().height();

            //Case of Syringe
            if((mario->isLookingLeft() && consumable->getTypeConsumable() == SYRINGE)) {
                if(mario->getState() == SMALL){
                    QSound::play(":/sounds/ressources/consumable_eat.wav");
                    mario->setState(NORMAL);
                    mario->setPixmap(mario->getMarioLeft());
                    //Adjust the position of mario to make him not fall
                    mario->setY(y - mario->pixmap().height() - 1);
                }
            }
            if((mario->isLookingRight() && consumable->getTypeConsumable() == SYRINGE)) {
                if(mario->getState() == SMALL){
                    QSound::play(":/sounds/ressources/consumable_eat.wav");
                    mario->setState(NORMAL);
                    //Set the pixmap if mario was SMALL
                    mario->setPixmap(mario->getMarioRight());
                    //Adjust the position of mario to make him not fall
                    mario->setY(y - mario->pixmap().height()-1);
                }
            }

            //Case of Star
            if((mario->isLookingLeft() && consumable->getTypeConsumable() == STAR)) {
                mario->setState(INVINCIBLE);
                //Set the pixmap
                mario->setPixmap(mario->getMarioLeft());
                mario->setHitMonster(true);
                //Adjust the position of mario to make him not fall
                mario->setY(y - mario->pixmap().height()-1);
            }
            if((mario->isLookingRight() && consumable->getTypeConsumable() == STAR)) {
                mario->setState(INVINCIBLE);
                //Set the pixmap
                mario->setPixmap(mario->getMarioRight());
                mario->setHitMonster(true);
                //Adjust the position of mario to make him not fall
                mario->setY(y - mario->pixmap().height()-1);
            }

            //Remove the consumable from the list and the scene
            listConsumable->removeOne(consumable);
            scene->removeItem(consumable);
        }
    }
}

/* Method that check if a monster is dead */
void Game::monsterDead(Monster* monster) {
    monster->isDead(listMonsters, scene);
}

/* Method that resolves collision between each monster on the map */
void Game::checkMonsterColliding(Monster* monster) {
    //Check if the monster is colliding with an object
    for (int item = 0; item < listObjects->size(); item++) {
        QGraphicsPixmapItem *pixmapItem = listObjects->at(item);
        if (monster->collidesWithItem(pixmapItem)) {
            //Get the direction of the collision
            collision_dir dir = collisionDir.GetCollisionDirX(monster, pixmapItem);

            //Update of the walking direction of the monster
            if (dir == LEFT && monster->getMonsterType() == COVID) {
                monster->setLeft(false);
                monster->setRight(true);
            }

            if (dir == RIGHT && monster->getMonsterType() == COVID) {
                monster->setLeft(true);
                monster->setRight(false);
            }
        }
    }

    //Check if the monster is colliding with an other monster
    for (int m = 0; m < listMonsters->size(); m++) {
        if (monster != listMonsters->at(m)) {
            QGraphicsPixmapItem *pixmapItem = listMonsters->at(m);
            if (monster->collidesWithItem(pixmapItem)) {
                //Get the direction of the collision
                collision_dir dir = collisionDir.GetCollisionDirX(monster, pixmapItem);

                //Update of the walking direction of the monster
                if (dir == LEFT) {
                    if (monster->getMonsterType() == COVID && listMonsters->at(m)->getMonsterType() == COVID){
                        monster->setLeft(false);
                        monster->setRight(true);

                        listMonsters->at(m)->setLeft(true);
                        listMonsters->at(m)->setRight(false);
                    }
                    if (monster->getMonsterType() == COVID && listMonsters->at(m)->getMonsterType() == PANGOLIN){
                        monster->setLeft(false);
                        monster->setRight(true);

                        listMonsters->at(m)->setLeft(true);
                        listMonsters->at(m)->setRight(false);
                        listMonsters->at(m)->setPixmap(listMonsters->at(m)->pixmap().transformed(QTransform().scale(-1,1)));
                    }
                    if (monster->getMonsterType() == PANGOLIN && listMonsters->at(m)->getMonsterType() == COVID){
                        monster->setLeft(false);
                        monster->setRight(true);
                        monster->setPixmap(monster->pixmap().transformed(QTransform().scale(-1,1)));

                        listMonsters->at(m)->setLeft(true);
                        listMonsters->at(m)->setRight(false);
                    }
                }

                if (dir == RIGHT) {

                    if (monster->getMonsterType() == COVID && listMonsters->at(m)->getMonsterType() == COVID){
                        monster->setLeft(true);
                        monster->setRight(false);

                        listMonsters->at(m)->setLeft(false);
                        listMonsters->at(m)->setRight(true);
                    }
                    if (monster->getMonsterType() == COVID && listMonsters->at(m)->getMonsterType() == PANGOLIN){
                        monster->setLeft(true);
                        monster->setRight(false);

                        listMonsters->at(m)->setLeft(false);
                        listMonsters->at(m)->setRight(true);
                        listMonsters->at(m)->setPixmap(listMonsters->at(m)->pixmap().transformed(QTransform().scale(-1,1)));
                    }
                    if (monster->getMonsterType() == PANGOLIN && listMonsters->at(m)->getMonsterType() == COVID){
                        monster->setLeft(true);
                        monster->setRight(false);
                        monster->setPixmap(monster->pixmap().transformed(QTransform().scale(-1,1)));

                        listMonsters->at(m)->setLeft(false);
                        listMonsters->at(m)->setRight(true);
                    }
                }
            }
        }
    }
}

/* Method to check the collision or not with the ground or all the objects of the scene */
void Game::checkMarioFalling(){
    //Check if the caracter is colliding with the level
    bool no_collision = true;

    for (int item = 0; item < ground->size(); item++) {

        //Check the collision with the grounds
        if(mario->collidesWithItem(ground->at(item)))
        {
            //Set the pixmap for each case of state or looking direction of mario
            if (mario->isLookingRight() && !mario->isWalkingRight() && !mario->isWalkingLeft()){
                if(mario->getState() == SMALL){
                    mario->setPixmap(mario->getMarioSmallRight());
                }
                if(mario->getState() == NORMAL || mario->getState() == INVINCIBLE){
                    mario->setPixmap(mario->getMarioRight());
                }
            }
            if (mario->isLookingLeft() && !mario->isWalkingRight() && !mario->isWalkingLeft()){
                if(mario->getState() == SMALL){
                    mario->setPixmap(mario->getMarioSmallLeft());
                }
                if(mario->getState() == NORMAL || mario->getState() == INVINCIBLE){
                    mario->setPixmap(mario->getMarioLeft());
                }
            }
            collision_dir dir = collisionDir.GetCollisionDirX(mario, ground->at(item));
            bool down = collisionDir.GetCollisionDown(mario, ground->at(item), mario->getFallSpeed());
            bool up = collisionDir.GetCollisionUp(mario, ground->at(item), mario->getJumpSpeed());

            //Stop the moving of mario for different case
            if(dir == LEFT && !down && !up) {
                mario->setLeft(false);
                mario->setFall(true);
            }
            else if(dir == RIGHT && !down && !up) {
                mario->setRight(false);
                mario->setFall(true);
            }
            else {
                no_collision = false;
            }
        }
    }

    //Make mario falling or not
    mario->setFall(no_collision);

    //Check the collision or not for each objects same as the grounds
    for ( int item = 0; item < listObjects->size(); item++ ){
        QGraphicsPixmapItem *pixmapItem = listObjects->at(item);
        if(mario->collidesWithItem(pixmapItem))
        {
            collision_dir dir = collisionDir.GetCollisionDirX(mario, pixmapItem);
            bool down = collisionDir.GetCollisionDown(mario, pixmapItem, mario->getFallSpeed());
            bool up = collisionDir.GetCollisionUp(mario, pixmapItem, mario->getJumpSpeed());

            if(dir == LEFT && !down && !up) {
                mario->setLeft(false);
            }
            if(dir == RIGHT && !down && !up) {
                mario->setRight(false);
            }
            if(down){
                mario->setFall(false);
            }
            if(up){
                mario->setJumping(false);
                mario->setFall(true);
            }
        }
    }
}

/* Method that resolves the collision between mario and monsters */
void Game::checkMarioCollidingMonster() {
    //Check if Mario is colliding with monsters
    if (mario->getState() == INVINCIBLE){
        for (int m = 0; m < listMonsters->size(); m++) {
            Monster* monster = listMonsters->at(m);
            if(mario->collidesWithItem(monster) && monster->getDead() != true) {
                if (!monster->getHitMario()){
                    QSound::play(":/sounds/ressources/monster.wav");
                    if (monster->getMonsterType() == PANGOLIN) {
                         monster->setWalkSpeed(4);
                    }
                    monster->setDead();
                    mario->setHitMonster(true);
                }
                else {
                    monster->hitMario();
                }
            }
        }
        mario->invincible();
    }
    else {
        if(!mario->getHitMonster()) {
            for (int m = 0; m < listMonsters->size(); m++) {
                Monster* monster = listMonsters->at(m);
                if(mario->collidesWithItem(monster) && monster->getDead() != true) {
                    //Get the direction of the collision
                    collision_dir dir = collisionDir.GetCollisionDirX(mario, monster);
                    bool down = collisionDir.GetCollisionDown(mario, monster, mario->getFallSpeed());

                    if (!monster->getHitMario()){
                        //Pangolin case
                        if(checkMarioHitingPangolin(monster)){
                                QSound::play(":/sounds/ressources/monster.wav");
                                mario->setJumpMonster(true);
                                monster->setWalkSpeed(4);
                                mario->setFall(false);
                                monster->setDead();
                        }
                        else{
                            //Collision with the monster coming from the left
                            if (dir == LEFT && !down) {
                                if (mario->getState() == NORMAL) {
                                    QSound::play(":/sounds/ressources/small.wav");
                                    //Set the hit boolean of mario at true
                                    mario->setHitMonster(true);
                                    //Update of the mario state
                                    mario->setState(SMALL);
                                    mario->setPixmap(mario->getMarioSmallLeft());
                                }
                                else if(mario->getState() == SMALL && mario->getLifes() > 0) {
                                    mario->loseLife();
                                    life->loseLife();

                                    //Respawn Mario at the start of the level
                                    if (!marioHasRespawn) {
                                        marioHasRespawn = true;
                                    }
                                    spawn();
                                }
                                else if(mario->getState() == SMALL && mario->getLifes() == 0){
                                    mario->setState(DEAD);
                                }
                            }

                            //Collision with the monster coming from the right
                            if (dir == RIGHT && !down) {
                                if (mario->getState() == NORMAL) {
                                    QSound::play(":/sounds/ressources/small.wav");
                                    //Set the hit boolean of mario at true
                                    mario->setHitMonster(true);
                                    //Update of the mario state
                                    mario->setState(SMALL);
                                    mario->setPixmap(mario->getMarioSmallRight());
                                }
                                else if(mario->getState() == SMALL && mario->getLifes() > 0) {
                                    mario->loseLife();
                                    life->loseLife();

                                    //Respawn Mario at the start of the level
                                    if (!marioHasRespawn) {
                                        marioHasRespawn = true;
                                    }
                                    spawn();
                                }
                                else if(mario->getState() == SMALL && mario->getLifes() == 0){
                                    mario->setState(DEAD);
                                }
                            }

                            //Collision with the monster coming from the down
                            if (down && monster->getMonsterType() != PANGOLIN) {
                                QSound::play(":/sounds/ressources/monster.wav");
                                mario->setJumpMonster(true);
                                mario->setFall(false);

                                monster->setDead();
                            }
                        }
                    }
                }
            }
        }
        else {
            mario->hitMonster();
        }
    }
}

/* Method that check if mario as reach the end of each level */
void Game::checkMarioEndLevel()
{
    switch (current_level) {
        //level 1
        case 1:
            if(mario->x() > end_x_1 && state != ENDING_LEVEL){
                if (mario->collidesWithItem(ground->back())){
                    temp_switch_time = 0;
                    state = ENDING_LEVEL;
                }
                else {
                    mario->setJumping(false);
                    mario->setRight(false);
                    mario->setLeft(false);
                }
            }
            break;
        //level 2
        case 2 :
            if(mario->x() > end_x_2 && state != ENDING_LEVEL){
                if (mario->collidesWithItem(ground->back())){
                    temp_switch_time = 0;
                    state = ENDING_LEVEL;
                }
                else {
                    mario->setJumping(false);
                    mario->setRight(false);
                    mario->setLeft(false);
                }
            }
            break;
        //end level
        case 3:
            for (Monster *m : *listMonsters){
                if(m->getMonsterType() == PANGOLIN && state != ENDING_LEVEL){
                    if(m->getDead() && m->getLifeCounter() == 0){
                        temp_switch_time = 0;
                        state = ENDING_LEVEL;
                    }
                }
            }
            break;
    }
}

/* Method to check if mario fall in the void */
void Game::checkMarioFallUnderGround()
{
    if(mario->y() > 80){
        if(mario->getLifes() > 0){
            mario->loseLife();
            life->loseLife();

            //Respawn Mario at the start of the level
            mario->setState(SMALL);
            mario->setPixmap(mario->getMarioSmallRight());
            mario->setHitMonster(false);
            mario->setOpacity(1);
            if (!marioHasRespawn) {
                marioHasRespawn = true;
            }
            spawn();
        }
        else{
            mario->setState(DEAD);
        }
    }
}

/* Method that resolves the collision between mario and the pangolin */
bool Game::checkMarioHitingPangolin(Monster *monster)
{
    if(monster->getMonsterType() == PANGOLIN && !mario->collidesWithItem(ground->at(0))){
        //Check if mario is hiting the head of the Pangolin
        if(monster->isLookingLeft() &&
                mario->pos().x() + mario->boundingRect().width() <= monster->pos().x() + 15){
            //If mario is higher than the head of the pangolin, return true
            if(mario->boundingRect().y() + mario->boundingRect().height() < -3){
                return true;
            }
            else{
                return false;
            }
        }
        else if(monster->isLookingRight() &&
                mario->pos().x() >  monster->pos().x() + monster->pixmap().width() - 15){
            if(mario->boundingRect().y() + mario->boundingRect().height() < -3){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return true;
        }
    }
    else{
        return false;
    }
}

/* Method to display the message success and to pass to the next level except for the end level */
void Game::endingLevel()
{
    //Case of the end level
    if (current_level == 3){
        checkMarioFalling();
        mario->moveCharacter();
        engineBoxes1.stop();
        if (temp_switch_time == 0){

            //remove the UI
            scene->removeItem(pieces);
            scene->removeItem(pieces_text);
            scene->removeItem(life);
            scene->removeItem(life_text);
            scene->removeItem(boss_life_text);
            scene->removeItem(boss_life);
            success_text = new QGraphicsTextItem();
            success_text->setPlainText("IT WAS ENOUGH TO DRINK BLEACH !!!!");
            success_text->setDefaultTextColor(Qt::white);
            success_text->setFont(QFont("times", 28));

            //Create Trump pixmap and message
            QString trump_image = ":/ui/ressources/Trump.png";
            QPixmap trump = QPixmap(trump_image);
            trump_Message = new QGraphicsPixmapItem();
            trump_Message->setPixmap(trump);
            trump_Message->setPos(465, -108);
            trump_Message->setOpacity(0);
            scene->addItem(trump_Message);
            centerOn(trump_Message);

            //Add the message at the correct position on the view
            QPointF viewCenterPoint = mapToScene(QPoint(this->rect().x(), this->rect().y()));
            int pos_x = viewCenterPoint.x() + this->rect().width()/4 -  success_text->boundingRect().width() / 2;
            success_text->setX(pos_x);
            success_text->setY(-150);
            success_text->setOpacity(0);
            scene->addItem(success_text);
        }

        //Make the opacity increase progressivly
        if (temp_switch_time % 5 == 0){
            trump_Message->setOpacity(trump_Message->opacity()+0.03);
            success_text->setOpacity(success_text->opacity()+0.03);
        }

        //Remove the pangolin after a little time
        if(temp_switch_time == 15){
            for (Monster *m : *listMonsters){
                if(m->getMonsterType() == PANGOLIN){
                    scene->removeItem(m);
                }
            }
        }

        //Cancel all the movements of mario
        mario->setLeft(false);
        mario->setRight(false);
        mario->setJumping(false);

        temp_switch_time++;
    }
    else {
        //Display the succes message if the timer is at 0
        if(temp_switch_time == 0){
            success_text = new QGraphicsTextItem();
            success_text->setPlainText("SUCCESS");
            success_text->setDefaultTextColor(Qt::white);
            success_text->setFont(QFont("times", 56));

            //Add the message at the correct position on the view
            QPointF viewCenterPoint = mapToScene(QPoint(this->rect().x(), this->rect().y()));
            int pos_x = viewCenterPoint.x() + this->rect().width()/4 -  success_text->boundingRect().width() / 2;
            success_text->setX(pos_x);
            success_text->setY(-200);
            scene->addItem(success_text);
        }

        //Check if mario is on the ground before switching to the next level
        for (int item = 0; item < ground->size(); item++) {
            if(mario->collidesWithItem(ground->at(item)) && temp_switch_time > total_switch_time){
                //Remove the message from the scene
                scene->removeItem(success_text);

                //Clean the scene, set up the level, and spawn it
                current_level++;
                if (marioHasRespawn) {
                    marioHasRespawn = false;
                }
                spawn();
            }
            else if(!mario->collidesWithItem(ground->at(item))){
                //Cancel all the movements of mario until he hits the ground
                mario->setLeft(false);
                mario->setRight(false);
                mario->setJumping(false);
                mario->setFall(true);
            }
            else if(mario->collidesWithItem(ground->at(item))){
                mario->setLeft(false);
                mario->setRight(false);
                mario->setJumping(false);
                mario->setFall(false);
            }
        }

        //Increment the current duration of the switch screen
        temp_switch_time += 16;
    }
}

/* Method to generate a level */
void Game::spawn()
{
    //Remove all items from the previous scene
    for (int item = 0; item < listObjects->size(); item++) {
        scene->removeItem(listObjects->at(item));
    }
    for (int item = 0; item < listMonsters->size(); item++) {
        scene->removeItem(listMonsters->at(item));
    }
    for (int item = 0; item < listConsumable->size(); item++) {
        scene->removeItem(listConsumable->at(item));
    }
    for (int item = 0; item < listBoxes->size(); item++) {
        scene->removeItem(listBoxes->at(item));
    }
    for (int item = 0; item < ground->size(); item++) {
        scene->removeItem(ground->at(item));
    }

    //Case of levels after the first level (remove the hospital and landscape)
    if ((current_level != 1 && !marioHasRespawn) || (current_level == 1 && marioHasRespawn)) {
        scene->removeItem(niveau->getHospital());
        scene->removeItem(niveau->getHillBush1());
    }

    //remove mario
    scene->removeItem(mario);

    //Create the current level
    niveau = new Level(scene, current_level);

    //Set the state of the game
    state = PLAYING;

    //For each level clear the lists and regenerate them
    switch (current_level) {
        case 1:
            this->listObjects->clear();
            this->listObjects = niveau->getListObjects_1();
            this->listBoxes->clear();
            this->listBoxes = niveau->getListBoxes_1();
            this->listMonsters->clear();
            this->listMonsters = niveau->getListMonsters_1();
            this->ground->clear();
            this->ground->push_back(niveau->GetGround_1());

            mario->setPos(mario_spawn_x_1, mario_spawn_y);
            break;

        case 2:
            this->listObjects->clear();
            this->listObjects = niveau->getListObjects_2();
            this->listBoxes->clear();
            this->listBoxes = niveau->getListBoxes_2();
            this->listMonsters->clear();
            this->listMonsters = niveau->getListMonsters_2();
            this->ground->clear();
            this->ground->push_back(niveau->GetGround_2_1());
            this->ground->push_back(niveau->GetGround_2_2());

            mario->setPos(mario_spawn_x_1, mario_spawn_y);
            break;

        case 3:
            this->listObjects->clear();
            this->listBoxes->clear();
            this->listMonsters->clear();

            if (marioHasRespawn) {
                scene->removeItem(boss_life);
                scene->removeItem(boss_life_text);
            }

            this->listMonsters = niveau->getListMonsters_end();
            this->listBoxes = niveau->getListBoxes_end();
            this->ground->clear();
            this->ground->push_back(niveau->GetGround_end());
            this->listObjects = niveau->getListObjects_end();
            mario->setPos(mario_spawn_x_2, mario_spawn_y);

            scene->addItem(boss_life_text);
            scene->addItem(boss_life);
            break;
    }

    //Add mario into the scene
    scene->addItem(mario);

    //Clear the list of consumable
    this->listConsumable->clear();
}

/* Method that quit the game if the player press Back space*/
void Game::closeGame()
{
    //Delete/clean all the pointer from the game
    delete scene;
    delete niveau;
    delete listObjects;
    delete listBoxes;
    delete listMonsters;
    delete listConsumable;
    delete ground;

    QApplication::quit();
}

/* Method to manage all the press event */
void Game::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Right)
    {
        mario->setRight(true);
    }
    if(event->key() == Qt::Key_Left)
    {
        mario->setLeft(true);
    }
    if((!mario->isJumping() && !mario->isFalling()) && (event->key() == Qt::Key_Space || event->key() == Qt::Key_Up))
    {
        if (mario->getState() == NORMAL || mario->getState() == INVINCIBLE) {
            QSound::play(":/sounds/ressources/jump_normal.wav");
        }
        if (mario->getState() == SMALL) {
            QSound::play(":/sounds/ressources/jump_small.wav");
        }
        mario->setJumping(true);
    }
    if((mario->isWalkingLeft() || mario->isWalkingRight()) && !mario->isFalling() && !mario->isJumping() && event->key() == Qt::Key_Shift)
    {
        mario->setRunning(true);
    }
    if(mario->getState() != DEAD && event->key() == Qt::Key_Escape){
        togglePause();
    }
    if(event->key() == Qt::Key_Backspace){
        closeGame();
    }
}

/* Method to manage all the release event */
void Game::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Right)
    {
        mario->setRight(false);
    }
    if(event->key() == Qt::Key_Left)
    {
         mario->setLeft(false);
    }
    if(event->key() == Qt::Key_Shift)
    {
        mario->setRunning(false);
    }
}
